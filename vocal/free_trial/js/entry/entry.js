$(function(){
	"use strict";
	
	MVHeight();
	
	$(window).load(function () {
		MVHeight();
		//ブラウザ
		
		if(!window.sessionStorage.getItem('slide_position')||window.sessionStorage.getItem('slide_position') === null){
			
		} else {
			setTimeout(function(){
				var set = window.sessionStorage.getItem('slide_position');
				window.sessionStorage.clear('slide_position');
				//alert(window.sessionStorage.getItem('slide_position'));
				$(".bx-pager-item:nth-child(" + set +") a").click();
				return false;
			},10);
		}
				
		//確認するボタンクリック
		$(document).on("click", "#stepnext05", function(){
			window.sessionStorage.setItem('mail_check', 1);
		});
				
		window.addEventListener("pagehide", errorMail);
		window.addEventListener("blur", errorMail);
		
		$('.bx-viewport').css('height',($('.l-step01').offset().top) - ($('header').outerHeight()) - ($('.l-mvBlock').outerHeight()) + 200 + 'px');
		
		//スライダーの設定
		$('.slider').bxSlider({
			auto: false,
			touchEnabled: false,
			infiniteLoop: false
		});
		
		////step01////
		//お名前-必須チェック
		$('.p-namae').keyup(function() {
			NamaeCheck();
		});
		//ご年代-必須チェック
		$('.p-age').change(function() {
			AgeCheck();
		});	
		//同意する-必須チェック
		$(document).on("click", ".l-agreeBlock label", function(){
			AgreeCheck();
		});		
		//電話番号-必須&電話番号チェック
		$('.p-tel').keyup(function() {
			TelCheck();
		});
		//メールアドレス-必須&メールアドレスチェック
		$('.p-mail').keyup(function() {
			MailCheck();
		});
		//step01の次へをクリック	
		$(document).on("click", ".js-step01", function(){
			var name = $(this).data('name');
			var number = $(this).data('number');
			setTimeout(function(){
				NamaeCheck();
				AgeCheck();
				AgreeCheck();
				TelCheck();
				MailCheck();
				var err_check01 = 0;
				//どこかでエラーがあれば次のステップに行けないようにする
				if(!$(".p-namae-err").hasClass('hid')){
					err_check01 = 1;
				}
				if(!$(".p-age-err").hasClass('hid')){
					err_check01 = 1;
				}
				if(!$(".p-checkbox01-err").hasClass('hid')){
					err_check01 = 1;
				}
				if(!$(".p-tel-err").hasClass('hid')){
					err_check01 = 1;
				}
				if(!$(".p-mail-err").hasClass('hid')){
					err_check01 = 1;
				}			
				if(err_check01 === 0){
					SlideCheck(name,number);
					//window.sessionStorage.setItem('slide_position', 2);
					$('.bx-viewport').css('height',($('.l-step02').offset().top) - ($('header').outerHeight()) - ($('.l-mvBlock').outerHeight()) + 200 + 'px');
				}
			},200);
		});	
		
		////step02////
		if($(".p-area-input01").prop('checked')) {
			$('.checkbox03-input').prop({'disabled':false});
			$('.l-area-detail label').removeClass('dis');
		}
		if($(".p-area-input02").prop('checked')) {
			$('.checkbox03-input').prop('checked', false);
			$('.checkbox03-input').prop({'disabled':true});
			$('.l-area-detail label').addClass('dis');
		}
		//希望エリア-「希望エリアを選択する」を選択した場合、チェックボックスを押せるようにする
		$(document).on("click", ".p-area-input01", function(){
			$('.checkbox03-input').prop({'disabled':false});
			$('.l-area-detail label').removeClass('dis');
		});
		//希望エリア-「相談して決める」を選択した場合、チェックボックスのチェックをはずす&チェックボックスを押せなくする
		$(document).on("click", ".p-area-input02", function(){
			$('.checkbox03-input').prop('checked', false);
			$('.checkbox03-input').prop({'disabled':true});
			$('.l-area-detail label').addClass('dis');
		});
		//step02の次へをクリック	
		$(document).on("click", ".js-step02", function(){
			var name = $(this).data('name');
			var number = $(this).data('number');
			setTimeout(function(){
				AreaCheck();
				var err_check02 = 0;
				//どこかでエラーがあれば次のステップに行けないようにする
				if(!$(".p-area-err").hasClass('hid')){
					err_check02 = 1;
				}			
				if(err_check02 === 0){
					SlideCheck(name,number);
					//window.sessionStorage.setItem('slide_position', 3);
					$('.bx-viewport').css('height',($('.l-step03').offset().top) - ($('header').outerHeight()) - ($('.l-mvBlock').outerHeight()) + 200 + 'px');
				}
			},200);
		});
		
		////step03////
		if($(".area3-input:eq(0)").prop('checked')) {
			$('.p-kibou1_1').prop({'disabled':false});
			$('.p-kibou2_1').prop({'disabled':false});
			$('.p-kibou3_1').prop({'disabled':false});
			
			$('.p-kibou').removeClass('dis');
		}
		if($(".area3-input:eq(1)").prop('checked')) {
			$('.p-kibou1_1').prop({'disabled':true});
			$('.p-kibou2_1').prop({'disabled':true});
			$('.p-kibou3_1').prop({'disabled':true});
			
			$('.p-kibou').addClass('dis');
		}
		//希望受講日時-「日付を決める」を選択した場合、textを押せるようにする
		$(document).on("click", ".p-area3-input01", function(){
			$('.area3-input:eq(0)').prop('checked', true);
			$('.p-kibou1_1').prop({'disabled':false});
			$('.p-kibou2_1').prop({'disabled':false});
			$('.p-kibou3_1').prop({'disabled':false});
			
			$('.p-kibou').removeClass('dis');
		});
		//希望受講日時-「相談して決める」を選択した場合、textを押せないようにする
		$(document).on("click", ".p-area3-input02", function(){
			$('.area3-input:eq(1)').prop('checked', true);
			$('.p-kibou1_1').prop({'disabled':true});
			$('.p-kibou2_1').prop({'disabled':true});
			$('.p-kibou3_1').prop({'disabled':true});
			
			$('.p-kibou').addClass('dis');
		});
		//step03の次へをクリック	
		$(document).on("click", ".js-step03", function(){
			var name = $(this).data('name');
			var number = $(this).data('number');
			
			if($(".p-area3-input02").prop('checked')) {
				setTimeout(function(){
					SlideCheck(name,number);
					$('.bx-viewport').css('height',($('.l-step04').offset().top) - ($('header').outerHeight()) - ($('.l-mvBlock').outerHeight()) + 200 + 'px');
				},200);
			} else {
				setTimeout(function(){
					Kibou1Check();
					Kibou2Check();
					var err_check03 = 0;
					//どこかでエラーがあれば次のステップに行けないようにする
					if(!$(".p-kibou1-err").hasClass('hid')){
						err_check03 = 1;
					}
					if(!$(".p-kibou2-err").hasClass('hid')){
						err_check03 = 1;
	
					}			
					if(err_check03 === 0){
						SlideCheck(name,number);
						//window.sessionStorage.setItem('slide_position', 4);
						$('.bx-viewport').css('height',($('.l-step04').offset().top) - ($('header').outerHeight()) - ($('.l-mvBlock').outerHeight()) + 200 + 'px');
					}
				},200);
			}
		});
		
		$(document).on("click", ".js-back", function(){
			var number = $(this).data('number');
			setTimeout(function(){
				$(".bx-pager-item:nth-child(" + number + ") a").click();
				$('.bx-viewport').css('height',($('.l-step0'+ number).offset().top) - ($('header').outerHeight()) - ($('.l-mvBlock').outerHeight()) + 200 + 'px');
			},200);
		});
		
	});
	
	$(window).resize(function () {
		setTimeout(function(){
		//サイズ調整
			MVHeight();
		},100);				
	});
	
});

////step01関連ファンクション////
function  NamaeCheck(){
	"use strict";
	if ($('.p-namae').val() === ""){
		//textの背景色を変更
		$(".p-namae").addClass("err");
		//エラーメッセージを表示
		$(".p-namae-err").removeClass("hid");
		$(".p-namae-err").text("※お名前は必須です");
	} else {
		//textの背景色を戻す
		$(".p-namae").removeClass("err");
		//エラーメッセージを非表示
		$(".p-namae-err").addClass("hid");
	}	
}
function  AgeCheck(){
	"use strict";
	if ($('.p-age').val() === ""){
		//selectの背景色を変更
		$(".p-age").addClass("err");
		//エラーメッセージを表示
		$(".p-age-err").removeClass("hid");
		$(".p-age-err").text("※ご年代は必須です");
		$('.l-agree').hide();
		$('.bx-viewport').css('height',($('.l-step01').offset().top) - ($('header').outerHeight()) - ($('.l-mvBlock').outerHeight()) + 200 + 'px');
	} else {
		
		if($('.p-age').val() === '～９歳'||$('.p-age').val() === '１０代'){
			//ご年代が「～9歳」or「10代」の時には同意ブロックを表示する
			$('.l-agree').show();
			$('.bx-viewport').css('height',($('.l-step01').offset().top) - ($('header').outerHeight()) - ($('.l-mvBlock').outerHeight()) + 200 + 'px');
		} else {
			$('.l-agree').hide();
			$('.bx-viewport').css('height',($('.l-step01').offset().top) - ($('header').outerHeight()) - ($('.l-mvBlock').outerHeight()) + 200 + 'px');
		}
		
		//selectの背景色を戻す
		$(".p-age").removeClass("err");
		//エラーメッセージを非表示
		$(".p-age-err").addClass("hid");
	}
}
function  AgreeCheck(){
	"use strict";
	if($('.checkbox01-input').is(':checked')) {
		//チェックされている
		//selectの背景色を戻す
		$(".l-agreeBlock label").removeClass("err");
		//エラーメッセージを非表示
		$(".p-checkbox01-err").addClass("hid");
	} else {
		//チェックされていない		
		//ご年代が「選択してください」の時に
		var val = $('[name=age]').val();
		if(val === ''){
			//selectの背景色を変更
			$(".l-agreeBlock label").addClass("err");
			//エラーメッセージを表示
			$(".p-checkbox01-err").removeClass("hid");
			$(".p-checkbox01-err").text("※ご年代を選択してください");
			//$(".p-checkbox01-err").text("※ご年代によって同意が必須となりますのでご年代を選択してください");
		} else if(val === '～９歳'||val === '１０代'){
			//ご年代が「～9歳」or「10代」の時には必須になる
			//selectの背景色を変更
			$(".l-agreeBlock label").addClass("err");
			//エラーメッセージを表示
			$(".p-checkbox01-err").removeClass("hid");
			$(".p-checkbox01-err").text("※ご年代が20歳未満の場合同意は必須です");
		} else {
			//selectの背景色を戻す
			$(".l-agreeBlock label").removeClass("err");
			//エラーメッセージを非表示
			$(".p-checkbox01-err").addClass("hid");
		}
	}
}
function  TelCheck(){
	"use strict";
	if ($('.p-tel').val() === ""){
		//textの背景色を変更
		$(".p-tel").addClass("err");
		//エラーメッセージを表示
		$(".p-tel-err").removeClass("hid");
		$(".p-tel-err").text("※電話番号は必須です");
	} else {
		if(!$('.p-tel').val().match(/^[0-9]+$/)) {
			//半角数字以外のものが含まれているのでエラー表示
			//textの背景色を変更
			$(".p-tel").addClass("err");
			//エラーメッセージを表示
			$(".p-tel-err").removeClass("hid");
			$(".p-tel-err").text("※電話番号は半角数字（ハイフンなし）で入力してください。例）08012345678");
		} else {
			//全角のみなのでエラーなし
			//textの背景色を戻す
			$(".p-tel").removeClass("err");
			//エラーメッセージを非表示
			$(".p-tel-err").addClass("hid");
		}
	}	
}
function  MailCheck(){
	"use strict";
	if ($('.p-mail').val() === ""){
		//textの背景色を変更
		$(".p-mail").addClass("err");
		//エラーメッセージを表示
		$(".p-mail-err").removeClass("hid");
		$(".p-mail-err").text("※メールアドレスは必須です");
	} else {
		if(!$('.p-mail').val().match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/)) {
			//メールアドレスの形式ではないのでエラー表示
			//textの背景色を変更
			$(".p-mail").addClass("err");
			//エラーメッセージを表示
			$(".p-mail-err").removeClass("hid");
			$(".p-mail-err").text("※メールアドレスの形式が正しくありません。");
		} else {
			//全角のみなのでエラーなし
			//textの背景色を戻す
			$(".p-mail").removeClass("err");
			//エラーメッセージを非表示
			$(".p-mail-err").addClass("hid");
		}
	}	
}

////step02関連ファンクション////
function  AreaCheck(){
	"use strict";
	//希望エリア-「希望エリアを選択する」を選択した場合にどのエリアもチェックされていない時にエラー出す。
	if($('.p-area-input01').is(':checked')) {
		var check_count = $('.l-area-detail label :checked').length;
		//チェックされている
		if (check_count === 0 ){
			//エラーメッセージを表示
			$(".p-area-err").removeClass("hid");
			$(".p-area-err").text("※エリアを選択してください");
		} else {
			//エラーメッセージを非表示
			$(".p-area-err").addClass("hid");
		}
		
	} else {
		//チェックされていない			
		//エラーメッセージを非表示
		$(".p-area-err").addClass("hid");
	}
}

////step03関連ファンクション////
function  Kibou1Check(){
	"use strict";
	if ($('.p-kibou1_1').val() === ""){
		//textの背景色を変更
		$(".p-kibou1_1").addClass("err");
		//エラーメッセージを表示
		$(".p-kibou1-err").removeClass("hid");
		$(".p-kibou1-err").text("※第一希望は必須です");
	} else {		
		//textの背景色を戻す
		$(".p-kibou1_1").removeClass("err");
		//エラーメッセージを非表示
		$(".p-kibou1-err").addClass("hid");
	}	
}
function  Kibou2Check(){
	"use strict";
	if ($('.p-kibou2_1').val() === ""){
		//textの背景色を変更
		$(".p-kibou2_1").addClass("err");
		//エラーメッセージを表示
		$(".p-kibou2-err").removeClass("hid");
		$(".p-kibou2-err").text("※第二希望は必須です");
	} else {		
		//textの背景色を戻す
		$(".p-kibou2_1").removeClass("err");
		//エラーメッセージを非表示
		$(".p-kibou2-err").addClass("hid");
	}	
}

function  SlideCheck(name,number){
	"use strict";
	//alert(name);
	$(".bx-pager-item:nth-child(" + number + ") a").click();
	var slide_position = $('.slider').offset().top - $('header').outerHeight();
	$('body,html').animate({
		scrollTop: slide_position
	}, 10);
	return false;	
}

function  errorMail(){
	"use strict";
	var mailcheck03 = $('.js-mailcheck03').attr('aria-hidden');
	var mailcheck04 = $('.js-mailcheck04').attr('aria-hidden');
	var mailcheck05 = $('.js-mailcheck05').attr('aria-hidden');			
	if(mailcheck03 === 'false' || mailcheck04 === 'false' || mailcheck05 === 'false'){
		if(!window.sessionStorage.getItem('mail_check')){
			// 初回なのでsessionStorageに値を格納
			window.sessionStorage.setItem('mail_check', 1);
			var hostUrl= '../error/';
			var exit_name = $('.p-namae').val();
			var exit_age = $('.p-age').val();					
			var exit_agree = '';
			var val = $('[name=age]').val();
			if(val === '～９歳'||val === '１０代'){
				exit_agree = '同意する';
			} else {
				exit_agree = '';
			}					
			var exit_tel = $('.p-tel').val();
			var exit_mail = $('.p-mail').val();
			var exit_other = $('.p-other').val();
			$.ajax({
				url: hostUrl,
				type:'POST',
				dataType: 'json',
				data : {namae : exit_name, age : exit_age, agree : exit_agree, tel : exit_tel, mail : exit_mail, other : exit_other },
				timeout:10000,
				async: false
			}).done(function(data) {
				
			}).fail(function(XMLHttpRequest, textStatus, errorThrown) {
				
			})
			return false;
		} else {
			
		}
	}
}

function  MVHeight(){
	"use strict";
	$('.l-mvBlock').css('padding-top', $('header').outerHeight() + 'px');
}

