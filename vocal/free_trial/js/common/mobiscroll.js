$(function () {
    "use strict";        
	
	
	//ドラムロール関連	
	//-----------------------------------------------
	
	//モーダル部分上部-日付部分の初期表示はnowを表示する
	var curr = new Date();
	var curr_new = curr.getFullYear() + '年' + (curr.getMonth()+1) + '月' + curr.getDate() + '日';
	$('.p-date > div').text(curr_new);
	
	//ドラムロールの設定
	ShowClock();
	
	//textがクリックされるまでモーダルは非表示
	$(".modal").hide();
	$("html").css('overflow','auto');
	$("html").css('height','100%');
	$("body").css('overflow','auto');
	$("body").css('height','auto');
	
	//textをクリック
	$(document).on('click', '#kibou1_1, #kibou2_1, #kibou3_1', function(){
		$("header").css('display','none');
		var name = $(this).attr('name');
		// セッションの格納
		window.sessionStorage.setItem(['kibou_name'],[name]);
		ShowModal();
	});
	
	//キャンセルボタンをクリックした時、値は変えずにモーダルを閉じる
	$(".js-cancel").click(function(){
		$(".modal").hide();
		$("body").css('overflow','auto');
		$("body").css('height','auto');
		$("header").css('display','block');
	});
	
	//OKボタンをクリックした時、各項目を取得してテキストに反映する
	$(".js-ok").click(function(){
		//反映処理
		GetTime();
		
		//モーダルを閉じる
		$(".modal").hide();
		$("body").css('overflow','auto');
		$("body").css('height','auto');
		$("header").css('display','block');
	});
	
	//ドラムロール以外の部分を押した場合はモーダルを閉じる
	$(document).on('click','.modal-back, .js-close', function(){
		$(".js-cancel").click();
	});
	
	$(window).on('load', function(){
		
		setTimeout(function(){
			//ドラムロール部分のスクロールがあればモーダル部分上部-日付部分を更新する
			$('.dwwr .dwcc > .dwsc').on('DOMSubtreeModified propertychange', function() {
				SetTime();		
			});
			
			$('<span class="while">～</span>').insertAfter('.dwc:nth-of-type(2)');
			
			//リサイズ時のモーダルレイアウトを修正する
			$('.modal').css('width',window.innerWidth + 'px');
			$('.modal').css('height',window.innerHeight + 'px');
			$('.modal-back').css('height',window.innerHeight + 'px');
			
			var widimage = window.innerWidth;
			
			if( widimage < 481 ){			
				$('.sense-ui .dw').css('width',window.innerWidth - 42 + 'px');
				$('.modal-cell > div').css('width',window.innerWidth - 42 + 'px');
				$('.dw.dwbg.dwi').css('width',window.innerWidth - 42 + 'px');
			} else if( widimage < 769 ){
				$('.sense-ui .dw').css('width',window.innerWidth - 84 + 'px');
				$('.modal-cell > div').css('width',window.innerWidth - 84 + 'px');
				$('.dw.dwbg.dwi').css('width',window.innerWidth - 84 + 'px');
			} else {
				$('.sense-ui .dw').css('width',window.innerWidth - 84 + 'px');
				$('.modal-cell > div').css('width',window.innerWidth - 84 + 'px');
				$('.dw.dwbg.dwi').css('width',window.innerWidth - 84 + 'px');
			}
			//$('.dw.dwbg.dwi').css('width',100 + '%');			
			var clock_width = $('.modal-cell div.p-clock').width();
			$('.modal-cell div.p-date').css('width',clock_width + 'px');
			$('.modal-cell div.p-button').css('width',clock_width + 'px');
		},200);
	});
	
	$(window).on('resize', function(){				
		setTimeout(function(){
			//リサイズ時のモーダルレイアウトを修正する
			$('.modal').css('width',window.innerWidth + 'px');
			$('.modal').css('height',window.innerHeight + 'px');
			$('.modal-back').css('height',window.innerHeight + 'px');
			
			var widimage = window.innerWidth;
				
			if( widimage < 481 ){			
				$('.sense-ui .dw').css('width',window.innerWidth - 42 + 'px');
				$('.modal-cell > div').css('width',window.innerWidth - 42 + 'px');
				$('.dw.dwbg.dwi').css('width',window.innerWidth - 42 + 'px');
			} else if( widimage < 769 ){
				$('.sense-ui .dw').css('width',window.innerWidth - 84 + 'px');
				$('.modal-cell > div').css('width',window.innerWidth - 84 + 'px');
				$('.dw.dwbg.dwi').css('width',window.innerWidth - 84 + 'px');
			} else {
				$('.sense-ui .dw').css('width',window.innerWidth - 84 + 'px');
				$('.modal-cell > div').css('width',window.innerWidth - 84 + 'px');
				$('.dw.dwbg.dwi').css('width',window.innerWidth - 84 + 'px');
			}
			var clock_width = $('.modal-cell div.p-clock').width();
			$('.modal-cell div.p-date').css('width',clock_width + 'px');
			$('.modal-cell div.p-button').css('width',clock_width + 'px');
		},100);
	});
	
	//-----------------------------------------------
});

function  SetTime(){
	"use strict";
	
	setTimeout(function(){
		
		//年月日
		var day = $('.dwwr .dwcc > .dwsc:nth-of-type(1) table tr td:nth-of-type(1) .dw-bf .dw-sel');
		day = day.text();
		
		var minute_from = $('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(2) .dw-bf .dw-sel');
		minute_from = minute_from.text();
		minute_from = minute_from.replace("時", ":");
		minute_from = minute_from.replace("分", "");
	
		//追加部分の時間to（分）
		var minute_to = $('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(2) .dw-bf .dw-sel');
		minute_to = minute_to.text();
		minute_to = minute_to.replace("時", ":");
		minute_to = minute_to.replace("分", "");		
		
		//ドラムロール部分の年月日をモーダル部分上部-日付部分に反映
		var new_date = day + minute_from + '～' + minute_to;
		$('.p-date > div').html(new_date);
	
	},100);
}

function  ShowClock(){
	"use strict";
	//ドラムロール初期設定	
	var curr = new Date().getFullYear();
	var opt = {

	}
	//「年/月/日/時/分」型
	opt.date = {preset : 'datetime'};
	//「分(60)/stepMinute(5.5) = 約11」で時間Fromと時間Toで表示できる数を設定
	opt.datetime = { preset : 'datetime', stepMinute: 5.5};
	//「yyyy/mm/dd」型（仮）
	opt.dateFormat = { preset : 'yyyy/mm/dd' };
	opt.time = {preset : 'time'};
	
	//ドラムロールの初期日付を「1990/1/1」に設定（中身は入れ替えるのでこれは仮）
	var today = new Date();
	var toay_new = 1653 + '/' + 1 + '/' + 1;
	//var toay_new = today.getFullYear() + '/' + (today.getMonth()+1) + '/' + today.getDate();
	
	//ドラムロールの詳細設定
	$('.p-clock').val(toay_new + ' 00:00').scroller().scroller($.extend(opt['datetime'], { theme: 'sense-ui', mode: 'scroller', display: 'inline', lang: 'ja' }));
	
	//後ろにToのドラムロールを設定
	TimeBlockAdd();
}

function  TimeBlockAdd(){
	"use strict";
	$(document).ready(function(){
		//プラグインでは「年/月/日/時/分」だけなので「年/月/日/時/分/時/分」にしてそのうち「日/分/分」を活用
		var baseObj = $('.dwwr .dwcc > .dwsc');
		var count = 0;
		$('.dwwr .dwcc > .dwsc').each( function(){
			count = count + 1;
			if(count === 2){
			$(this).insertAfter('.dwwr .dwcc > .dwsc');
			}
		});
	});
}

function  GetTime(){
	"use strict";
	
	//年月日（年）var year = $('.dwwr .dwcc > .dwsc:nth-of-type(1) table tr td:nth-of-type(1) .dw-bf .dw-sel');
	//年月日（月）var month = $('.dwwr .dwcc > .dwsc:nth-of-type(1) table tr td:nth-of-type(2) .dw-bf .dw-sel');
	
	//年月日
	var day = $('.dwwr .dwcc > .dwsc:nth-of-type(1) table tr td:nth-of-type(1) .dw-bf .dw-sel');
	day = day.text();
		
	//時間from（時）var hour_form = $('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(1) .dw-bf .dw-sel');
	
	//時間from（分）
	var minute_from = $('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(2) .dw-bf .dw-sel');
	minute_from = minute_from.text();
	minute_from = minute_from.replace("時", ":");
	minute_from = minute_from.replace("分", "");
	
	//追加部分の時間to（時）var hour_to = $('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(1) .dw-bf .dw-sel');
	
	//追加部分の時間to（分）
	var minute_to = $('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(2) .dw-bf .dw-sel');
	minute_to = minute_to.text();
	minute_to = minute_to.replace("時", ":");
	minute_to = minute_to.replace("分", "");
	
	var current1 = '';
	
	//セッションの値の取得（どこの希望に時間を反映させるか）
	current1 = window.sessionStorage.getItem(['kibou_name']);
	
	//current1 = 'kibou1_1';
	
	//それぞれのテキストに表示する
	var input1 = day;	
	var input2 = ' ' + minute_from + '～';
	var input3 = minute_to;	
	$('input[name="'+ current1 +'"]').val(input1 + input2 + input3);
}

function  ShowModal(){
	"use strict";
	
	//モーダルを表示
	$(".modal").show();
	$("body").css('overflow','hidden');
	$("body").css('height','100%');
	//ドラムロール部分の表示
	$(".dw-inline").fadeIn();
	//ドラムロール部分の幅を設定
	//$('.sense-ui .dw').css('width',window.innerWidth - 10 + 'px');
	//モーダルは画面全体に表示
	$('.modal').css('height',window.innerHeight + 'px');
	$('.modal-back').css('height',window.innerHeight + 'px');
	
	//デフォルトで存在する年と月は非表示
	//$('.dwwr .dwcc > .dwsc:nth-of-type(1) table tr td:nth-of-type(1)').css('display','none');
	$('.dwwr .dwcc > .dwsc:nth-of-type(1) table tr td:nth-of-type(2)').css('display','none');
	$('.dwwr .dwcc > .dwsc:nth-of-type(1) table tr td:nth-of-type(3)').css('display','none');
	
	//デフォルトの「日、分、分」をそれぞれ「年月日、時間From、時間To」に活用
	$('.dwwr .dwcc > .dwsc:nth-of-type(1) table tr td:nth-of-type(1) .dwl').html('<b>日付</b>');
	$('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(2) .dwl').html('<b>レッスン受講可能時間</b>' + '<br>' + '<span class="p-caution">※赤色の時間帯は[<span class="p-red">土・日</span>]、青色の時間帯は' + '<br>' + '[<span class="p-blue">月～金</span>]のみ選択可能です。</span>');
	$('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(2) .dwl').text(' ');
	
	//年月日部分の設定（365日分）
	var i = 0;		
	$('.dwwr .dwcc > .dwsc:nth-of-type(1) table tr td:nth-of-type(1) .dw-li').each( function(){
		var wNames = ['日', '月', '火', '水', '木', '金', '土'];
		var nowDate = new Date();
		//本日から　i　日後のDate
		var futureDate = new Date(nowDate.getTime() + i*24*60*60*1000);			
		var txt = "" + futureDate.getFullYear() + "年" + (futureDate.getMonth()+1) + "月" + futureDate.getDate() + "日(" + wNames[futureDate.getDay()] + ")";
		$(this).find('.dw-i').text(txt);
		i = i + 1;			
	});
	
	//時間Fromに文字列を設定
	$('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(1):nth-of-type(1)').css('display','none');
	//$('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(2)').css('display','inline-block');
	$('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(1) .dw-i').text('10時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(2) .dw-i').text('11時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(3) .dw-i').text('12時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(4) .dw-i').text('13時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(5) .dw-i').text('14時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(6) .dw-i').text('15時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(7) .dw-i').text('16時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(8) .dw-i').text('17時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(9) .dw-i').text('18時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(10) .dw-i').text('19時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(2) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(11) .dw-i').text('20時30分');
	
	var test_pad = $('.dwc:nth-of-type(2)').outerHeight()/2 + 16;
	$('.while').css('top',test_pad + 'px');
	
	//時間Toに文字列を設定
	$('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(1):nth-of-type(1)').css('display','none');
	//$('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(2)').css('display','inline-block');
	$('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(1) .dw-i').text('11時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(2) .dw-i').text('12時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(3) .dw-i').text('13時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(4) .dw-i').text('14時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(5) .dw-i').text('15時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(6) .dw-i').text('16時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(7) .dw-i').text('17時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(8) .dw-i').text('18時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(9) .dw-i').text('19時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(10) .dw-i').text('20時30分');
	$('.dwwr .dwcc > .dwsc:nth-of-type(3) table tr td:nth-of-type(2) .dw-bf:nth-of-type(1) > div:nth-of-type(11) .dw-i').text('21時30分');
	
	//モーダル部分上部-日付部分とボタン部分の幅をドラムロール部分の幅と同じになるように設定
	var clock_width = $('.modal-cell div.p-clock').width();
	$('.modal-cell div.p-date').css('width',clock_width + 'px');
	$('.modal-cell div.p-button').css('width',clock_width + 'px');
	
	setTimeout(function(){
		//ボタンのレイアウト設定
		$('.p-button > div > p > input').matchHeight();
	},200);
}