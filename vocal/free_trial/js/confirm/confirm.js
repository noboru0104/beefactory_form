$(function(){
	"use strict";
	
	MVHeight();
	
	$(window).load(function () {
		MVHeight();
		window.sessionStorage.setItem('slide_position', 4);
		
		//送信ボタンクリック
		$(document).on("click", "#stepnextcomplete", function(){
			window.sessionStorage.setItem('mail_check2', 1);
		});
		
		window.addEventListener("pagehide", errorMail);
		window.addEventListener("blur", errorMail);
		
		
	});
	
	$(window).resize(function () {
		setTimeout(function(){
		//サイズ調整
			MVHeight();
		},100);				
	});
	
});

function  errorMail(){
	"use strict";
	if(!window.sessionStorage.getItem('mail_check2')){
		//alert('test1');
		// 初回なのでsessionStorageに値を格納
		window.sessionStorage.setItem('mail_check2', 1);
		var hostUrl= '../error/';
		var exit_name = $('.p-namae2').val();
		var exit_age = $('.p-age2').val();					
		var exit_agree = '';
		//var val = $('[name=age]').val();
		if(exit_age === '～９歳'||exit_age === '１０代'){
			exit_agree = '同意する';
		} else {
			exit_agree = '';
		}					
		var exit_tel = $('.p-tel2').val();
		var exit_mail = $('.p-mail2').val();
		var exit_other = $('.p-other2').val();
		$.ajax({
			url: hostUrl,
			type:'POST',
			dataType: 'json',
			data : {namae : exit_name, age : exit_age, agree : exit_agree, tel : exit_tel, mail : exit_mail, other : exit_other },
			timeout:10000,
			async: false
		}).done(function(data) {
						  //alert("ok2");
		}).fail(function(XMLHttpRequest, textStatus, errorThrown) {
						 //alert("error2");
		})
		return false;
	} else {
		
	}
}

function  MVHeight(){
	"use strict";
	$('.l-mvBlock').css('padding-top', $('header').outerHeight() + 'px');
}
