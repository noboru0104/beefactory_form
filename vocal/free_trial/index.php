<?php
$ua = $_SERVER['HTTP_USER_AGENT'];// ユーザエージェントを取得
if(isSmartPhone($ua)) {
	include 'sp.php';
} else {
	include 'pc.php';
}
function isSmartPhone($ua) {
	if((strpos($ua, 'iPhone') !== false) || (strpos($ua, 'iPod') !== false) ||(strpos($ua, 'Android') !== false)) return true;
	return false;
}?>