<?php
define('DEBUG_MODE',false);
if(DEBUG_MODE) {
	ini_set('display_errors',1);
} else {
	ini_set('display_errors',0);
}
define('TYPE','vocal');
define('DOC_ROOT',$_SERVER['DOCUMENT_ROOT']);
define('LIB_DIR',DOC_ROOT.'/'.TYPE.'/assets/form_lib/');
if(is_file(LIB_DIR.'define.php')) { require_once(LIB_DIR.'define.php'); } else { print('error'); exit; }
if(is_file(LIB_DIR.'function.php')) { require_once(LIB_DIR.'function.php'); } else { print('error'); exit; }

//--------------------------------------------------
// テンプレート定義
//--------------------------------------------------
define('TPL_DIR',DOC_ROOT.'/'.TYPE.'/free_trial/');
define('TPL_IPT',TPL_DIR.'input.php');
define('TPL_CNF',TPL_DIR.'confirm.php');
define('TPL_THX','./thanks.php');
define('TPL_ERR','./error.html');

//--------------------------------------------------
// フォーム個別設定
//--------------------------------------------------
$prefix = 'cf-';
$name_list = array(
	'cf-school_flag'=>array('lbl'=>'希望エリア','required'=>true,'option'=>array("新宿","渋谷","池袋","赤羽","まだ決まっていないので相談したい")),
	'cf-date_flag'=>array('lbl'=>'受講希望日','required'=>false,'option'=>array(" ","まだ決まっていないので相談したい")),
	'cf-date'=>array('lbl'=>'第一希望日','required'=>true),
	'cf-date2'=>array('lbl'=>'第二希望日','required'=>true),
	'cf-date3'=>array('lbl'=>'第三希望日','required'=>false),
	//第一希望
	'cf-time'=>array('lbl'=>'第一希望時間','required'=>true,'option'=>array("選択してください","時間はどこでも良い","10時30分（土・日のみ）","11時30分（土・日のみ）","12時30分","13時30分","14時30分","15時30分","16時30分","17時30分","18時30分","19時30分（月〜金のみ）","20時30分（月〜金のみ）")),
	'cf-time1-2'=>array('lbl'=>'第一希望時間終了時間','required'=>false,'option'=>array("選択してください","11時30分（土・日のみ）","12時30分（土・日のみ）","13時30分","14時30分","15時30分","16時30分","17時30分","18時30分","19時30分","20時30分（月〜金のみ）","21時30分（月〜金のみ）")),

	//第二希望
	'cf-time2'=>array('lbl'=>'第二希望時間','required'=>true,'option'=>array("選択してください","時間はどこでも良い","10時30分（土・日のみ）","11時30分（土・日のみ）","12時30分","13時30分","14時30分","15時30分","16時30分","17時30分","18時30分","19時30分（月〜金のみ）","20時30分（月〜金のみ）")),
	'cf-time2-2'=>array('lbl'=>'第二希望時間終了時間','required'=>false,'option'=>array("選択してください","11時30分（土・日のみ）","12時30分（土・日のみ）","13時30分","14時30分","15時30分","16時30分","17時30分","18時30分","19時30分","20時30分（月〜金のみ）","21時30分（月〜金のみ）")),

	//第三希望
	'cf-time3'=>array('lbl'=>'希第三希望時間','required'=>false,'option'=>array("選択してください","時間はどこでも良い","10時30分（土・日のみ）","11時30分（土・日のみ）","12時30分","13時30分","14時30分","15時30分","16時30分","17時30分","18時30分","19時30分（月〜金のみ）","20時30分（月〜金のみ）")),
	'cf-time3-2'=>array('lbl'=>'第三希望時間終了時間','required'=>false,'option'=>array("選択してください","11時30分（土・日のみ）","12時30分（土・日のみ）","13時30分","14時30分","15時30分","16時30分","17時30分","18時30分","19時30分","20時30分（月〜金のみ）","21時30分（月〜金のみ）")),

	'cf-name'=>array('lbl'=>'お名前','required'=>true,'maxlen'=>255),
	'cf-age'=>array('lbl'=>'ご年代','required'=>true,'option'=>array("選択してください","〜９歳","10代","20代","30代","40代","50代","60代以上")),
	'cf-tel'=>array('lbl'=>'電話番号','required'=>true,'valid'=>'tel','maxlen'=>15,'format'=>'tel'),
	'cf-mail'=>array('lbl'=>'メールアドレス','required'=>true,'valid'=>'mail','maxlen'=>256,'format'=>'mail'),
	'cf-mess'=>array('lbl'=>'お問い合わせ内容','required'=>false,'maxlen'=>5000),
	'ip'=>array('lbl'=>'IP','required'=>false,'maxlen'=>5000),
	'ua'=>array('lbl'=>'ua','required'=>false,'maxlen'=>5000),
	'cf-agree'=>array('lbl'=>'同意','required'=>true)
);
//--------------------------------------------------------------------------------
// メール設定
//--------------------------------------------------------------------------------
// 送信元メールアドレス
$mail_from = array(
	'address' => 't.nakagawa0314@gmail.com',
	'name'    => '株式会社ビー・ファクトリー'
);
$mail_return_path = 't.nakagawa0314@gmail.com';
// t.nakagawa0314@gmail.com
// 管理者メールアドレス
$mail_adm_to = array('t.nakagawa0314@gmail.com');
$mail_adm_cc = array();


// お問い合わせ - 個人
//--------------------------------------------------
// ユーザー宛 メール件名
$mail_sbj_usr = "【株式会社ビー・ファクトリー】問い合わせ受付のお知らせ";
// ユーザー宛 メール本文
$mail_msg_usr =<<<_MAIL_TO_USR
%cf-name%　様

この度は、Beeボーカルスクールへのお問い合わせをいただきまして、誠にありがとうございます。
お問い合わせ内容を確認させていただき、ご回答させていただきます。
なお、お問い合わせの内容によっては、ご回答まで数日かかる場合がございます。
恐れ入りますが、ご連絡まで今しばらくお待ちいただきますよう、お願いいたします。

■ 無料レッスン希望スクールについて
希望エリア：%cf-school-str%
希望受講日時：%cf-date-str%

■ お客様情報
お名前：%cf-name%
ご年代：%cf-age-str%
メールアドレス：%cf-mail%
お電話番号：%cf-tel%
ご希望欄：
%cf-mess%

------------------------------------------------------------------
株式会社ビー・ファクトリー
〒171-0021　東京都豊島区西池袋5-4-7　池袋トーセイビル4F
TEL:03-6914-1815（月～金 11：00～22：00 土・日　11:00～20:00）
https://www.bee-music.jp/

_MAIL_TO_USR;


// お問い合わせ - 管理者
//--------------------------------------------------
// 管理者宛 メール件名
$mail_sbj_adm = '【Webサイト】無料体験レッスン（ボーカル）のお問い合わせがありました。';
// 管理者宛 メール文言
$mail_msg_adm =<<<_MAIL_TO_ADM

Beeボーカルスクールより以下のとおり、問い合わせがありました。
お問い合わせ内容をご確認ください。

■ 無料レッスン希望スクールについて
希望エリア：%cf-school-str%
希望受講日時：%cf-date-str%

■ お客様情報
お名前：%cf-name%
ご年代：%cf-age-str%
メールアドレス：%cf-mail%
お電話番号：%cf-tel%
ご希望欄：
%cf-mess%

■ 補足情報
未成年の保護者同意確認：同意する
IP：%ip%
デバイス：%ua%

------------------------------------------------------------------
株式会社ビー・ファクトリー
https://www.bee-music.jp/

_MAIL_TO_ADM;

//--------------------------------------------------
// メインルーチン
//--------------------------------------------------
$task = isset($_POST['_task']) && $_POST['_task']!='' ? $task=$_POST['_task'] : '';
if(empty($task)){
	Csrf::create();
	$csrf_token = Csrf::fetch();
}else{
	if(Csrf::check()) {
		$csrf_token = Csrf::fetch();
	}else{
		$task = '';
	}
}
$e_mess = array();
switch($task) {

	case 'send':// メール送信・サンクスページ
		$vars = encodeFormValues($_POST);
		defValEmpty($vars,$name_list);
		option2strAll($vars,$name_list);
		$e_mess = formValid($vars,$name_list);
		if($vars['cf-date_flag']=='2'){
			//var_dump($vars['cf-date_flag']);
			if(isset($e_mess['mess']['cf-date'])){
				unset($e_mess['mess']['cf-date']);
				$e_mess['count'] = $e_mess['count']-1;
			}
			if(isset($e_mess['mess']['cf-time'])){
				unset($e_mess['mess']['cf-time']);
				$e_mess['count'] = $e_mess['count']-1;
			}
			if(isset($e_mess['mess']['cf-date2'])){
				unset($e_mess['mess']['cf-date2']);
				$e_mess['count'] = $e_mess['count']-1;
			}
			if(isset($e_mess['mess']['cf-time2'])){
				unset($e_mess['mess']['cf-time2']);
				$e_mess['count'] = $e_mess['count']-1;
			}
		}

		if($e_mess['count']>0) {
			include(TPL_IPT);
		} else {
			formatter($vars,$name_list);
			valArray2Str($vars,$name_list,', ');
			$vars['date'] = time();
			// 例外処理
			if(isset($vars['cf-school_flag'])){
				if($vars['cf-school_flag']=='2'){
					$vars['cf-school-str'] = $vars['cf-school_flag-str'];
				}else{
					$vars['cf-school-str'] = $vars['cf-school_flag-str'];
				}

			}

			if(isset($vars['cf-date_flag'])){

				if($vars['cf-date_flag']=='2'){
					$vars['cf-date-str'] = $vars['cf-date_flag-str'];
				}else{

					if($vars['cf-time1-2'] == '' || $vars['cf-time1-2'] == '1'){
						$cf_time1_2 = '';
					} else {
						$cf_time1_2 = '～'.($vars['cf-time1-2-str']);
					}
					if($vars['cf-time2-2'] == '' || $vars['cf-time2-2'] == '1'){
						$cf_time2_2 = '';
					} else {
						$cf_time2_2 = '～'.$vars['cf-time2-2-str'];
					}
					if($vars['cf-time3-str'] == '選択してください'){
						$cf_time3_2 = '';
					} else {
						$cf_time3_2 = $vars['cf-time3-str'];
					}
					if($vars['cf-time3-2'] == '' || $vars['cf-time3-2'] == '1'){
						$cf_time3_2 .= '';
					} else {
						$cf_time3_2 .= '～'.$vars['cf-time3-2-str'];
					}
					$vars['cf-time3-str'] = $cf_time3_2;
					$vars['cf-date-str'] = "第一希望：　".$vars['cf-date'].' '.$vars['cf-time-str'].$cf_time1_2 ."\n第二希望：　". $vars['cf-date2'].' '.$vars['cf-time2-str']." ".$cf_time2_2 ."\n第三希望：　". $vars['cf-date3'].' '.$vars['cf-time3-str'];
				}

			}

	require($_SERVER['DOCUMENT_ROOT'].'/assets/ip.php');
	$ipkey = in_array($vars['ip'], $array_ip);
    if($ipkey){
    } else {
			// if(!DEBUG_MODE) {
				// メール送信 ユーザ
				if(!mailSender2($vars, $mail_from, $mail_return_path, $vars[$prefix.'mail-str'], $mail_sbj_usr, $mail_msg_usr)) {
					header('Location: '.TPL_ERR); exit;
				}
				// メール送信 管理者
				for($i=0; $i<count($mail_adm_to); $i++) {
					if(!mailSender2($vars, $mail_from, $mail_return_path, $mail_adm_to[$i], $mail_sbj_adm, $mail_msg_adm, $mail_adm_cc)) {
						header( 'Location: '.TPL_ERR); exit;
					}
				}
			// }
	}
			unset($vars);
			$task = '';
			// $csrf_token = '';
			// include(TPL_THX);
			header('Location: '.TPL_THX.'?token='.$csrf_token);
		}
	break;

	case 'check':// 入力確認・確認ページ
		$vars = varsInit($name_list,encodeFormValues($_POST));
		defValEmpty($vars,$name_list);
		option2strAll($vars,$name_list);
		$e_mess = formValid($vars,$name_list);
		// 例外処理
		if($vars['cf-school_flag']=='2'){
			if(isset($e_mess['mess']['cf-school'])){
				unset($e_mess['mess']['cf-school']);
				$e_mess['count'] = $e_mess['count']-1;
			}
		}
		if($vars['cf-date_flag']=='2'){
			//var_dump($vars['cf-date_flag']);
			if(isset($e_mess['mess']['cf-date'])){
				unset($e_mess['mess']['cf-date']);
				$e_mess['count'] = $e_mess['count']-1;
			}
			if(isset($e_mess['mess']['cf-time'])){
				unset($e_mess['mess']['cf-time']);
				$e_mess['count'] = $e_mess['count']-1;
			}
			if(isset($e_mess['mess']['cf-date2'])){
				unset($e_mess['mess']['cf-date2']);
				$e_mess['count'] = $e_mess['count']-1;
			}
			if(isset($e_mess['mess']['cf-time2'])){
				unset($e_mess['mess']['cf-time2']);
				$e_mess['count'] = $e_mess['count']-1;
			}
		}
		if($e_mess['count']>0) {
			include(TPL_IPT);
		} else {
			formatter($vars,$name_list);
			$_hiddens = mkHidden($vars,$name_list);
			include(TPL_CNF);
		}
	break;

	case 'reset':// リセット
		unset($vars);
		foreach($name_list as $key => $val){ if(!isset($vars[$key])) { $vars[$key] = ''; } }
		include(TPL_IPT);
	break;

	case 'back':// 戻る
		$vars = varsInit($name_list,encodeFormValues($_POST));
		defValEmpty($vars,$name_list);
		include(TPL_IPT);
	break;

	default:// 入力
		$vars = varsInit($name_list);
		include(TPL_IPT);
}
if(DEBUG_MODE) {
	print('<pre style="text-align:left;">');
	print_r($vars);
	print('</pre>');
}
?>