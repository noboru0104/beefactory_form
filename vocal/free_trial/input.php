<!DOCTYPE html>
<html lang="ja" xmlns="https://www.w3.org/1999/xhtml" xmlns:og="https://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
<?php require($_SERVER['DOCUMENT_ROOT'].'/vocal/include/analytics_head_start.php'); ?>

<title>45分無料体験レッスン | ボイストレーニングのBeeボーカルスクール(新宿・池袋・渋谷)</title>

<?php require($_SERVER['DOCUMENT_ROOT'].'/vocal/include/head.php'); ?>

<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" >
<script src="/vocal/assets/js/datepicker-ja.js"></script>
<script src="/vocal/assets/js/script.js"></script>
<style>
	textarea{
		width: 100%;
		box-sizing: border-box;
	}
	#aside #aside-inner ul.bnrs-side{
		margin-top: 0;
	}
	#fix-contact-pc .container .inq{
		width: 510px;
		margin: 0 auto;
		float: none;
	}
	#fix-contact-pc .container .inq dl dd.tel{
		text-align: center;
	}
	#fix-contact-sp ul li:first-of-type{
		width: 63%;
		color: #fff;
		line-height: 1.25;
	}
	#fix-contact-sp ul li:first-of-type a{
		display: block;
		margin-top: 2px;
	}
	#fix-contact-sp ul li:last-of-type{
		margin-top: 17px;
		float: right;
	}
	.hissu{
		background-color:#cc0000;
		color:#FFFFFF;
		padding:3px 8px;
		display:inline;
		font-size:10px;
		margin-right:5px;
		position:relative;
		top:-1px;
	}
	.nini{
		background-color:#6489c1;
		color:#FFFFFF;
		padding:3px 8px;
		display:inline;
		font-size:10px;
		margin-right:5px;
		position:relative;
		top:-1px;
	}
	.form.contact select:disabled{
		background-color:#dddddd;
	}
	.time-to{
		width:141px;
	}

	.date_margin{
		margin: 0 0 15px;
	}
	.icon_margin{
		margin: 5px 0 0;
		line-height:1.5;
	}
	.hasDatepicker{
		margin-right:4px;
	}

	input[type="text"],
	select{
		width: 20em;
		box-sizing: border-box;
	}
	input[type="text"].is-date{
		width: 13em;
	}

	.ui-datepicker{
		width: 94%;
		max-width: 500px;
	}
	.ui-datepicker td{
		padding: 2px;
	}
	.ui-datepicker td a{
	    padding: 18% 10%;
	}
	@media screen and (max-width:640px){
		.date_margin{
			margin: 0 0 15px;
		}
		.icon_margin{
			margin: 5px 0 5px;
			line-height:1.5;
		}
		.hasDatepicker{
			margin-right:8px;
			margin-bottom:8px;
		}

		.time-to{
			font-size:93.7%;
			width:135px;
			padding:4px 0;
		}
		.select_age{
			display:block;
			font-size:93.7%;
			width: 100%;
			padding:4px 0;
		}
		input[type="text"]{
			width: 100%;
		}
	}
</style>
<script>
jQuery(function($){
	$("#datepicker").datepicker({
		showOn: "both",
		buttonImage: "/vocal/assets/img/free_trial/ico-calendar.png",
		buttonImageOnly: true,
		buttonText: "Select date"
	});
	$("#datepicker2").datepicker({
		showOn: "both",
		buttonImage: "/vocal/assets/img/free_trial/ico-calendar.png",
		buttonImageOnly: true,
		buttonText: "Select date"
	});
	$("#datepicker3").datepicker({
		showOn: "both",
		buttonImage: "/vocal/assets/img/free_trial/ico-calendar.png",
		buttonImageOnly: true,
		buttonText: "Select date"
	});
	$('#formSubmit').taskSubmit('check','form[name="form"]');

	if( $(window).width() < 640 ){
		$('input[type="text"], textarea').focusin(function(){
			$('#header, #fix-contact-sp').hide();
		});
		$('input[type="text"], textarea').focusout(function(){
			$('#header, #fix-contact-sp').show();
		});
	}
});
$(function(){
    var loc=false;
    $(window).bind("beforeunload", function(e) {
        // 確認メッセージに表示させたい文字列
        if (!loc) {
            return "本当に戻りますか？";
        }
    });
    // aリンクを遷移OKにする場合はこのコメントを外す
    $('a').click( function() {loc=true;});
    $("form").submit(function(){loc=true;});
});
$(function(){
	$atr = $('textarea').attr('placeholder');
	$(window).on('load, resize', function(){
		if( $(window).width() < 640 ){
			$('textarea').attr('placeholder','質問や要望などございましたら、気軽にご記入、ご相談下さい。例）「初心者です」「高い声を出せるようになりたい」「弾き語りをしたい」')
		} else {
			$('textarea').attr('placeholder',$atr)
		}
	});
});
</script>
</head>
<body id="top" class="vocal free-trial">

<div id="deqwas-collection-k" style="display:none"></div>
<div id="deqwas-k" style="display:none"></div>
<script type="text/javascript">
/*<![CDATA[*/
    var deqwas_k = { option: {} };
    deqwas_k.cid = 'beemusic';

    deqwas_k.cart_list = 'vocal';

    (function () {
        var script = document.createElement('script');
        script.src = (location.protocol == 'https:' ? 'https:' : 'http:') + '//kdex005.deqwas.net/beemusic/scripts/cart.js?noCache=' + (new Date()).getTime();
        script.type = 'text/javascript';
        script.defer = true;
        script.charset = 'UTF-8';
        document.getElementById('deqwas-k').appendChild(script);
    })();
/*]]>*/
</script>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQNZJ8P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="wrap">

<!-- ▽▽▽▽▽ header ▽▽▽▽▽ -->
<header id="header">

	<div class="lead">
		<div class="container">
			<p>歌がもっと上手くなりたいあなたのためのBeeボーカルスクール。新宿・渋谷・池袋・赤羽の駅から駅近＆キレイで、お子様や女性にも安心して通って頂けるボイストレーニング教室です。</p>
			<ul class="sns">
				<li><a href="https://www.youtube.com/channel/UCWglVipGXNMtXeHvv7Cv9GQ" target="_blank"><img src="/vocal/assets/img/parts/h-sns-youtube.png" alt="youtube"></a></li>
				<li><a href="https://www.facebook.com/beemusic.jp/" target="_blank"><img src="/vocal/assets/img/parts/h-sns-fb.png" alt="facebook"></a></li>
			</ul>
		</div>
	</div>
	<div class="desc">
		<div class="container">
			<div class="ttls">
				<p class="l-logo"><a href="/vocal/"><img src="/vocal/assets/img/parts/h-site-logo-vocal.png" alt="Beeボーカルスクール[Bee vocal school]" /></a></p>
			</div>
			<div class="inq">
				<dl>
					<dt>お電話による無料体験レッスンお申込み、お問い合わせはこちら</dt>
					<dd class="op">受付時間：<br>平日 9:30-21:30<br>土日 9:30-19:30</dd>
					<dd class="tel"><img src="/vocal/assets/img/parts/h-contact-tel.png" alt="[フリーダイヤル]0120-015-349(イコーミュージック)携帯電話からも通話可能"></dd>
				</dl>
			</div>
			<?php $referer = $_SERVER['HTTP_REFERER'];
			$url = parse_url($referer);
			if ($url['host'] === $_SERVER['REMOTE_HOST']) {
				echo '<a href="" id="nav-main-btn" data-tg="#nav-main"><span></span><span></span><span></span></a>';
			} else {
			}
			?>
		</div>
	</div>


	<!-- <p class="header-news"><span>お知らせ：</span>12月29日(金)～1月3日(水)は休業とさせていただきます。ご不便をお掛けしますが、何卒よろしくお願い申し上げます。</p> -->

	<nav class="nav-menu" id="nav-main">

		<ul class="container">
			<li class="nav-home"><a href="/vocal/"><span>ホーム</span></a></li>
			<li><a href="/vocal/about/"><span>当スクールについて</span></a></li>
			<li><a href="/vocal/cource/"><span>料金/コース</span></a></li>
			<li><a href="/vocal/staff/"><span>スタッフ紹介</span></a></li>
			<li><a href="/vocal/voice/"><span>生徒さんの声</span></a></li>
			<li><a href="/vocal/faq/"><span>よくある質問</span></a></li>
			<li><a href="/vocal/access/"><span>アクセスマップ</span></a></li>
			<li class="nav-site">
				<ul>
					<li><a href="/vocal/company/">会社概要</a></li>
					<li><a href="/vocal/privacy/">プライバシーポリシー</a></li>
					<li><a href="/vocal/ad/">取材・広告へのお問い合わせ</a></li>
					<li><a href="/vocal/sitemap/">サイトマップ</a></li>
				</ul>
			</li>
			<li class="nav-bee">
				<ul>
					<li><a href="/vocal/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-vocal.png" alt="Beeボーカルスクール"><span><span class="ico-arrow"></span>Beeボーカルスクール</span></a></li>
					<li><a href="/piano/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-piano.png" alt="Beeピアノスクール"><span><span class="ico-arrow"></span>Beeピアノスクール</span></a></li>
					<li><a href="/guitar/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-guiter.png" alt="Beeギタースクール"><span><span class="ico-arrow"></span>Beeギタースクール</span></a></li>
					<li><a href="/voice/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-voice.png" alt="Beeボイススクール"><span><span class="ico-arrow"></span>Beeボイススクール</span></a></li>
					<li><a href="/lp/dj/standard/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-dj.png" alt="BeeDJスクール"><span><span class="ico-arrow"></span>BeeDJスクール</span></a></li>
					<li><a href="/lp/ukulele/standard/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-ukulele.png" alt="Beeウクレレスクール"><span><span class="ico-arrow"></span>Beeウクレレスクール</span></a></li>
				</ul>
			</li>
		</ul>

	</nav>
</header>
<!-- △△△△△ header △△△△△ -->

<!-- ▽▽▽▽▽ contents ▽▽▽▽▽ -->
<article id="contents">
	<div class="container">

	<?php
	if ($url['host'] === $_SERVER['REMOTE_HOST']) {
		echo '
		<nav id="breadcrumb">
			<ol itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
				<li itemprop="child"><a href="/vocal/" itemprop="url"><span itemprop="title">Beeボーカルスクール</span></a></li>
				<li>45分無料体験レッスン</li>
			</ol>
		</nav>';
	} else {
	}
	?>

		<article id="wrapper">


			<aside id="aside" class="hajust-cts"><div id="aside-inner">
				<ul class="bnrs-side">
					<li><a href="https://recruit.bee-music.jp/" target="_blank"><img src="/vocal/assets/img/index/bnr01.jpg" alt="講師採用情報 - BeeMusicSchool採用サイトへ"></a></li>
					<!-- <li><a href="/vocal/campaign/" target="_blank" onclick="window.open(this.href,'campaign','width=960,height=800,resizable=yes,scrollbars=yes');return false;"><img src="/vocal/assets/img/index/bnr03.jpg" alt="[無料]レッスン3回無料キャンペーン"></a></li> -->
					<li><a href="/vocal/sns_accounts/" target="_blank" onclick="window.open(this.href,'sns_accounts','width=960,height=800,resizable=yes,scrollbars=yes');return false;"><img src="/vocal/assets/img/index/bnr05.jpg" alt="beeミュージックスクールSNSアカウント一覧"></a></li>
				</ul>
			</div></aside>


			<main id="main" class="hajust-cts"><div id="main-inner">

				<h1 class="page-ttl line1"><strong>45分無料体験レッスン</strong></h1>

				<section class="section">
					<p class="mt30 mb40"><img src="/vocal/assets/img/free_trial/trial-img-arrow.png" alt="さぁ、気軽に無料体験レッスンへ　あなたのストーリーはここから始まります。" class="img-c"></p>
				</section>

				<section class="section">
					<h2 class="midashi2">45分の体験無料レッスン お申し込みフォーム</h2>
					<p>無料体験レッスン希望のお客様は、以下のフォームに必要事項をご記入の上、「上記内容で申し込む」をクリックしてください。</p>
				</section>

				<section class="section" id="freeTrialForm">
					<form name="form" method="post" action="<?php echo (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>#freeTrialForm">
						<h3 class="midashi3">無料レッスン希望スクールについて</h3>
						<dl class="form contact">
							<dt>希望エリア</dt>
							<dd>
								無料体験レッスンを受けるスクールに希望ある場合選択してください。<br>
                                <span class="hissu">必須</span>複数選択可<br>
								<?php foreach($name_list['cf-school_flag']['option'] as $key => $value):?>
                                <?php if(($key+1) == 5){ ?>
                                <br>
                                <?php } ?>
                                <label><input class="erea_check erea<?php echo $key+1; ?>" type="checkbox" name="cf-school_flag[]" value="<?php echo $key+1; ?>"<?php echo in_array($key+1, $vars['cf-school_flag']) ? ' checked' : ''; ?>><span style="padding-left:3px;padding-right:10px;"><?php echo $value; ?></span></label>
                                <?php endforeach; ?><br>
<?php foreach($name_list['cf-school']['option'] as $key => $value):?>
									<option value="<?php echo $key+1; ?>"<?php echo isset($vars['cf-school'])&&$vars['cf-school']==$key+1 ? ' selected' : ''; ?>><?php echo $value; ?></option>
<?php endforeach; ?>
								<?php if(!empty($e_mess['mess']['cf-school_flag'])): ?><font color="red"><?php echo $e_mess['mess']['cf-school_flag']; ?></font><?php endif; ?>
							</dd>
							<dt>希望受講日時</dt>
							<dd>
								希望日時がある場合選択してください。<br>
								<input type="radio" name="cf-date_flag" class="date_radio" value="1" id="date_flag_1"<?php echo isset($vars['cf-date_flag'])&&$vars['cf-date_flag']=='1' ? ' checked' : (empty($vars['cf-date_flag']) ? ' checked' : ''); ?>>
								<label for="date_flag_1">日付を選択する</label><br>
								<div class="DateDisplay">
                                    <div id="entryDate" class="date_margin">
                                        <p class="icon_margin"><span class="hissu">必須</span>第一希望</p>
                                        <input readonly="readonly" name="cf-date" class="is-date" type="text" value="<?php echo isset($vars['cf-date']) ? $vars['cf-date'] : ''; ?>" id="datepicker" placeholder="アイコンから日付を選択">
                                        <div class="date_label">
                                            <select name="cf-time" class="is-date time-to" id="cf-time">
        <?php foreach($name_list['cf-time']['option'] as $key => $value):?>
                                            <option value="<?php echo $key+1; ?>"<?php echo isset($vars['cf-time'])&&$vars['cf-time']==$key+1 ? ' selected' : ''; ?>><?php echo $value; ?></option>
        <?php endforeach; ?>
                                            </select>
                                            ～
                                            <select name="cf-time1-2" id="cf-time1-2" class="time-to">
        <?php foreach($name_list['cf-time1-2']['option'] as $key => $value):?>
                                            <option value="<?php echo $key+1; ?>"<?php echo isset($vars['cf-time1-2'])&&$vars['cf-time1-2']==$key+1 ? ' selected' : ''; ?>><?php echo $value; ?></option>
        <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="date_margin">
                                        <p class="icon_margin"><span class="hissu">必須</span>第二希望</p>
                                        <input readonly="readonly" name="cf-date2" class="is-date" type="text" value="<?php echo isset($vars['cf-date2']) ? $vars['cf-date2'] : ''; ?>" id="datepicker2" placeholder="アイコンから日付を選択">
                                        <div class="date_label">
                                            <select name="cf-time2" class="is-date time-to" id="cf-time2">
        <?php foreach($name_list['cf-time2']['option'] as $key => $value):?>
                                            <option value="<?php echo $key+1; ?>"<?php echo isset($vars['cf-time2'])&&$vars['cf-time2']==$key+1 ? ' selected' : ''; ?>><?php echo $value; ?></option>
        <?php endforeach; ?>
                                            </select>
                                            ～
                                            <select name="cf-time2-2" id="cf-time2-2" class="time-to">
        <?php foreach($name_list['cf-time2-2']['option'] as $key => $value):?>
                                            <option value="<?php echo $key+1; ?>"<?php echo isset($vars['cf-time2-2'])&&$vars['cf-time2-2']==$key+1 ? ' selected' : ''; ?>><?php echo $value; ?></option>
        <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="date_margin">
                                        <p class="icon_margin"><span class="nini">任意</span>第三希望</p>
                                        <input readonly="readonly" name="cf-date3" class="is-date" type="text" value="<?php echo isset($vars['cf-date3']) ? $vars['cf-date3'] : ''; ?>" id="datepicker3" placeholder="アイコンから日付を選択">
                                        <div class="date_label">
                                            <select name="cf-time3" class="is-date time-to" id="cf-time3">
        <?php foreach($name_list['cf-time3']['option'] as $key => $value):?>
                                            <option value="<?php echo $key+1; ?>"<?php echo isset($vars['cf-time3'])&&$vars['cf-time3']==$key+1 ? ' selected' : ''; ?>><?php echo $value; ?></option>
        <?php endforeach; ?>
                                            </select>
                                            ～
                                            <select name="cf-time3-2" id="cf-time3-2" class="time-to">
        <?php foreach($name_list['cf-time3-2']['option'] as $key => $value):?>
                                            <option value="<?php echo $key+1; ?>"<?php echo isset($vars['cf-time3-2'])&&$vars['cf-time3-2']==$key+1 ? ' selected' : ''; ?>><?php echo $value; ?></option>
        <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

								<input type="radio" name="cf-date_flag" class="date_radio" value="2" id="date_flag_0"<?php echo isset($vars['cf-date_flag'])&&$vars['cf-date_flag']=='2' ? ' checked' : ''; ?>>
								<label for="date_flag_0">まだ決まっていないので相談したい</label>
								<?php if(!empty($e_mess['mess']['cf-date_flag'])): ?><font color="red"><?php echo $e_mess['mess']['cf-date_flag']; ?></font><?php endif; ?>
								<?php if(!empty($e_mess['mess']['cf-date'])): ?><font color="red"><?php echo $e_mess['mess']['cf-date']; ?></font><?php endif; ?>
								<?php if(!empty($e_mess['mess']['cf-time'])): ?><font color="red"><?php echo $e_mess['mess']['cf-time']; ?></font><?php endif; ?>
                                <?php if(!empty($e_mess['mess']['cf-date2'])): ?><font color="red"><?php echo $e_mess['mess']['cf-date2']; ?></font><?php endif; ?>
								<?php if(!empty($e_mess['mess']['cf-time2'])): ?><font color="red"><?php echo $e_mess['mess']['cf-time2']; ?></font><?php endif; ?>
							</dd>
						</dl>
						<h3 class="midashi3 mt20">お客様情報</h3>
						<dl class="form contact">
							<dt>お名前</dt>
							<dd>
								<span class="hissu">必須</span>
                                <input type="text" name="cf-name" size="40" value="<?php echo isset($vars['cf-name']) ? $vars['cf-name'] : ''; ?>" placeholder="例）やまだ　はなこ">
								<?php if(!empty($e_mess['mess']['cf-name'])): ?><font color="red"><?php echo $e_mess['mess']['cf-name']; ?></font><?php endif; ?>
							</dd>
							<dt>ご年代</dt>
							<dd>
								<span class="hissu">必須</span>
                                <select name="cf-age" class="select_age">
<?php foreach($name_list['cf-age']['option'] as $key => $value):?>
									<option value="<?php echo $key+1; ?>"<?php echo isset($vars['cf-age'])&&$vars['cf-age']==$key+1 ? ' selected' : ''; ?>><?php echo $value; ?></option>
<?php endforeach; ?>
								</select>
								<?php if(!empty($e_mess['mess']['cf-age'])): ?><font color="red"><?php echo $e_mess['mess']['cf-age']; ?></font><?php endif; ?>
							</dd>

							<dt>お電話番号</dt>
							<dd>
								<span class="hissu">必須</span>
                                <input type="text" name="cf-tel" value="<?php echo isset($vars['cf-tel']) ? $vars['cf-tel'] : ''; ?>" size="40" placeholder="例）09011112222">
								<?php if(!empty($e_mess['mess']['cf-tel'])): ?><font color="red"><?php echo $e_mess['mess']['cf-tel']; ?></font><?php endif; ?>
							</dd>
							<dt>メールアドレス</dt>
							<dd>
								<span class="hissu">必須</span>
                                <input type="text" name="cf-mail" value="<?php echo isset($vars['cf-mail']) ? $vars['cf-mail'] : ''; ?>" size="40" placeholder="例）sample@beemusic.jp">
								<?php if(!empty($e_mess['mess']['cf-mail'])): ?><font color="red"><?php echo $e_mess['mess']['cf-mail']; ?></font><?php endif; ?>
							</dd>
							<dt>ご希望欄</dt>
							<dd>
								お好きな音楽のジャンル、希望する講師の特徴、その他の体験レッスン受講希望日・曜日・時間帯、レッスン受講の動機など、ご要望やご質問などがございましたら、お気軽にご記入ください。<br>
								<span style="font-size: 11px;">※状況によって、ご期待に添いかねるケースがございますが、最大限対応させて頂きます。</span><br>
                                <span class="nini">任意</span><br>
								<textarea name="cf-mess" cols="60" rows="10" placeholder="質問や要望などございましたら、気軽にご記入、ご相談下さい。

例）「初心者です」
　　「高い声を出せるようになりたい」
　　「弾き語りをしたい」"><?php echo isset($vars['cf-mess']) ? $vars['cf-mess'] : ''; ?></textarea><br>
								<?php if(!empty($e_mess['mess']['cf-mess'])): ?><font color="red"><?php echo $e_mess['mess']['cf-mess']; ?></font><?php endif; ?>
							</dd>
							<dd>
								20歳以上の方はチェックを入れた上で、申し込みボタンを選択してください。未成年の方で、保護者の了承を得ている場合チェックしてください。<br>
								<span class="hissu">必須</span>
								<label for="cf-agree"><input type="checkbox" name="cf-agree" id="cf-agree"<?php if( $vars['cf-agree'] ) {echo ' checked';} ?>> 同意する</label>
								<?php if(!empty($e_mess['mess']['cf-agree'])): ?><font color="red">同意するをチェックしてください。</font><?php endif; ?>
							</dd>
						</dl>

						<div class="button">
							<input type="submit" value="上記内容で申し込む" id="formSubmit">
						</div>

						<input type="hidden" name="_task" value="">
						<input type="hidden" name="_csrf" value="<?php echo $csrf_token; ?>">
                      <script src="/assets/ipjs.php"></script>
                      <script>
                      document.write('<input type="hidden" value="' + ipAddress + '" name="ip">');
                      </script>
                      <?php
						$ua = $_SERVER['HTTP_USER_AGENT'];
						if ( preg_match('/Windows Phone/ui', $ua) ) { //UAにAndroidも含まれるので注意
						    $ua_txt = 'WindowsPhone';
						} else if ( preg_match('/Windows/', $ua) ) {
						    $ua_txt = 'Windows';
						} else if ( preg_match('/Macintosh/', $ua) ) {
						    $ua_txt = 'Macintosh';
						} else if ( preg_match('/iPhone/', $ua) ) {
						    $ua_txt = 'iPhone';
						} else if ( preg_match('/iPad/', $ua) ) {
						    $ua_txt = 'iPad';
						} else if ( preg_match('/iPod/', $ua) ) {
						    $ua_txt = 'iPod';
						} else if ( preg_match('/Android/', $ua) ) {
						    if ( preg_match('/Mobile/', $ua) ) {
						        $ua_txt = 'Android';
						    } else {
						        $ua_txt = 'AndroidTablet';
						    }
						}
						echo '<input type="hidden" name="ua" value="'.$ua_txt.'">';
                      ?>
					</form>
				</section>

			</div></main>

		</article>
	</div>
</article>
<!-- △△△△△ contents △△△△△ -->

<!-- ▽▽▽▽▽ footer ▽▽▽▽▽ -->
<footer id="footer">
	<?php
	if ($url['host'] === $_SERVER['REMOTE_HOST']) {
		echo '
	<nav class="nav-ctrl">
		<div class="container">
			<ul>
				<li><a href="/vocal/">ホーム</a></li>
				<li><a href="#top" class="pagetop">ページトップへ</a></li>
			</ul>
		</div>
	</nav>
	<nav class="nav-menu">
		<div class="container">
			<ul>
				<li><a href="/vocal/about/">スクールのご案内</a>
					<ul>
						<li><a href="/vocal/about/">ごあいさつ</a></li>
						<li><a href="/vocal/about/kodawari.html">当校のこだわり</a></li>
					</ul>
				</li>
				<li><a href="/vocal/about/feature/">当スクールの特徴</a>
					<ul>
						<li><a href="/vocal/about/feature/system.html">初心者も安心のシステム</a></li>
						<li><a href="/vocal/about/feature/free_time.html">フレキシブルタイム制</a></li>
						<li><a href="/vocal/about/feature/nomination.html">インストラクター指名制度</a></li>
						<li><a href="/vocal/about/feature/counseling.html">個別カウンセリング</a></li>
						<li><a href="/vocal/about/feature/studio.html">充実の設備</a></li>
						<li><a href="/vocal/about/feature/event.html">発表会＆イベント</a></li>
						<li><a href="/vocal/about/feature/audition.html">オーディション</a></li>
						<li><a href="/vocal/about/feature/backup.html">バックアップ体制</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="/vocal/cource/">料金/コース</a>
					<ul>
						<li><a href="/vocal/cource/beginner.html">初心者ボーカルコース</a></li>
						<li><a href="/vocal/cource/karaoke.html">カラオケ上達コース</a></li>
						<li><a href="/vocal/cource/overcome.html">音痴克服コース</a></li>
						<li><a href="/vocal/cource/band.html">バンドボーカルコース</a></li>
						<li><a href="/vocal/cource/pro.html">プロ志望コース</a></li>
						<li><a href="/vocal/cource/singwith.html">弾き語りコース</a></li>
						<li><a href="/vocal/cource/musical.html">声楽・ミュージカルコース</a></li>
						<li><a href="/vocal/cource/instructor.html">ボーカル講師養成コース</a></li>
						<li><a href="/vocal/cource/sns.html">動画サイト/SNSアップコース</a></li>
						<li><a href="/vocal/cource/adult.html">大人（40〜60代）のボーカルコース</a></li>
					</ul>
				</li>
				<li><a href="/vocal/staff/">スタッフ紹介</a>
					<ul>
						<li><a href="/vocal/staff/instructor.html">インストラクター</a></li>
						<li><a href="/vocal/staff/concierge.html">楽器コンシェルジュ</a></li>
						<li><a href="/vocal/staff/receptionist.html">レセプショニスト</a></li>
						<li><a href="/vocal/staff/counselor.html">レッスンカウンセラー</a></li>
						<li><a href="/vocal/staff/event_staff.html">イベントスタッフ</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="/vocal/about/equipment/">施設紹介</a>
					<ul>
						<li><a href="/vocal/about/equipment/lesson.html">レッスン風景</a></li>
						<li><a href="/vocal/about/equipment/rec.html">レコーディング風景</a></li>
						<li><a href="/vocal/about/equipment/school_studio.html">スクール＆スタジオ</a></li>
						<li><a href="/vocal/about/equipment/live.html">発表会</a></li>
					</ul>
				</li>
				<li><a href="/vocal/voice/">生徒さんの声</a>
					<ul>
						<li><a href="/vocal/voice/">生徒さんの声</a></li>
						<li><a href="/vocal/voice/success.html">活躍する生徒さん</a></li>
					</ul>
				</li>
			</ul>
			<ul>
				<li><a href="/vocal/faq/">よくある質問</a></li>
				<li><a href="/vocal/access/">アクセスマップ</a>
					<ul>
						<li><a href="/vocal/access/shinjuku_ac.html">新宿校</a></li>
						<li><a href="/vocal/access/shibuya_ac.html">渋谷校</a></li>
						<li><a href="/vocal/access/ikebukuro_hon_ac.html">池袋本校</a></li>
						<li><a href="/vocal/access/ikebukuro_ac.html">池袋校</a></li>
						<li><a href="/vocal/access/akabane_hon_ac.html">赤羽本校</a></li>
						<li><a href="/vocal/access/akabane_ac.html">赤羽駅前校</a></li>
						<li><a href="/vocal/access/akabane_minami_ac.html">赤羽南口校</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
	<nav class="nav-bee">
		<ul class="container">
			<li><a href="/vocal/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-vocal.png" alt="Beeボーカルスクール"><span><span class="ico-arrow"></span>Beeボーカルスクール</span></a></li>
			<li><a href="/guitar/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-guiter.png" alt="Beeギタースクール"><span><span class="ico-arrow"></span>Beeギタースクール</span></a></li>
			<li><a href="/piano/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-piano.png" alt="Beeピアノスクール"><span><span class="ico-arrow"></span>Beeピアノスクール</span></a></li>
			<li><a href="/voice/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-voice.png" alt="Beeボイススクール"><span><span class="ico-arrow"></span>Beeボイススクール</span></a></li>
			<li><a href="/lp/dj/standard/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-dj.png" alt="BeeDJスクール"><span><span class="ico-arrow"></span>BeeDJスクール</span></a></li>
			<li><a href="/lp/ukulele/standard/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-ukulele.png" alt="Beeウクレレスクール"><span><span class="ico-arrow"></span>Beeウクレレスクール</span></a></li>
		</ul>
	</nav>';
	} else {
	}
	?>
	<section class="infos">
		<div class="container">
	<?php
	if ($url['host'] === $_SERVER['REMOTE_HOST']) {
		echo '
			<nav class="nav-site">
				<ul>
					<li><a href="/vocal/company/">会社概要</a></li>
					<li><a href="/vocal/privacy/">プライバシーポリシー</a></li>
					<li><a href="/vocal/ad/">取材・広告へのお問い合わせ</a></li>
					<li><a href="/vocal/sitemap/">サイトマップ</a></li>
				</ul>
			</nav>';
	} else {
	}
	?>
			<dl class="info">
				<dt>ボイストレーニング・ボイトレの<br class="br-sp">Beeボーカルスクール（新宿・渋谷・池袋・赤羽）</dt>
				<dd class="copyright"><small>Copyright &copy; 2009-2015 Bee Vocal School.<br class="br-sp"> All Rights Reserved.</small></dd>
			</dl>
		</div>
	</section>
</footer>
<!-- △△△△△ footer △△△△△ -->

<article id="fix-contact-pc">
	<div class="container">
		<div class="inq">
			<dl>
				<dt>お電話による無料体験レッスンお申込み、お問い合わせはこちら</dt>
				<dd class="op"><span style="display: inline-block; float: left;">受付時間：</span>
				<span style="display: block; text-align: right; padding: 0 2em 0 0;">平日 9:30-21:30</span>
				<span style="display: block; text-align: right; padding: 0 2em 0 0;">土日 9:30-19:30</span></dd>
				<dd class="tel"><img src="/vocal/assets/img/parts/inq-tel-f.png" alt="[フリーダイヤル]0120-015-349(イコーミュージック)携帯電話からも通話可能"></dd>
			</dl>
		</div>
	</div>
</article>

<article id="fix-contact-sp">
	<ul>
		<li>受付時間：<br>平日 9:30-21:30　土日 9:30-19:30
			<a href="tel:0120-015-349"><img src="/vocal/assets/img/parts/inq-tel-f.png" alt="[フリーダイヤル]0120-015-349(イコーミュージック)携帯電話からも通話可能"></a></li>
		<li><a href="tel:0120-015-349" class="btn-def-white"><span>電話での<br>お問い合わせ</span></a></li>
	</ul>
</article>

</div><!-- //wrap -->

</body>
</html>
