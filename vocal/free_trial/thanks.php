<?php
define('DEBUG_MODE',false);
if(DEBUG_MODE) {
	ini_set('display_errors',1);
} else {
	ini_set('display_errors',0);
}
define('TYPE','vocal');
define('DOC_ROOT',$_SERVER['DOCUMENT_ROOT']);
define('LIB_DIR',DOC_ROOT.'/'.TYPE.'/assets/form_lib/');
if(is_file(LIB_DIR.'define.php')) { require_once(LIB_DIR.'define.php'); } else { print('error'); exit; }
if(is_file(LIB_DIR.'function.php')) { require_once(LIB_DIR.'function.php'); } else { print('error'); exit; }
if(! Csrf::checkHash()) {
	header('Location: ./');
	exit;
}
?><!DOCTYPE html>
<html lang="ja" xmlns="https://www.w3.org/1999/xhtml" xmlns:og="https://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
<?php require($_SERVER['DOCUMENT_ROOT'].'/vocal/include/analytics_head_start.php'); ?>

<title>45分無料体験レッスン | ボイストレーニングのBeeボーカルスクール(新宿・池袋・渋谷)</title>

<?php require($_SERVER['DOCUMENT_ROOT'].'/vocal/include/head.php'); ?>
</head>
<body id="top" class="vocal free-trial">

<div id="deqwas-collection-k" style="display:none"></div>
<div id="deqwas-k" style="display:none"></div>
<script type="text/javascript">
/*<![CDATA[*/
    var deqwas_k = { option: {} };
    deqwas_k.cid = 'beemusic';

    deqwas_k.order_id = '';
    deqwas_k.order_items = 'vocal';
    deqwas_k.order_total = 0;
    deqwas_k.order_quantity = 0;

    (function () {
        var script = document.createElement('script');
        script.src = (location.protocol == 'https:' ? 'https:' : 'http:') + '//kdex005.deqwas.net/beemusic/scripts/cv.js?noCache=' + (new Date()).getTime();
        script.type = 'text/javascript';
        script.defer = true;
        script.charset = 'UTF-8';
        document.getElementById('deqwas-k').appendChild(script);
    })();
/*]]>*/
</script>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQNZJ8P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="wrap">

<?php require($_SERVER['DOCUMENT_ROOT'].'/vocal/include/header.php'); ?>

<!-- ▽▽▽▽▽ contents ▽▽▽▽▽ -->
<article id="contents">
	<div class="container">

		<nav id="breadcrumb">
			<ol itemscope itemtype="https://data-vocabulary.org/Breadcrumb">
				<li itemprop="child"><a href="/vocal/" itemprop="url"><span itemprop="title">Beeボーカルスクール</span></a></li>
				<li>45分無料体験レッスン</li>
			</ol>
		</nav>

		<article id="wrapper">

			<aside id="aside" class="hajust-cts"><div id="aside-inner">
				<nav class="nav-side">
					<ul>
						<li><a href="/vocal/free_trial/">45分無料体験レッスン</a></li>
					</ul>
				</nav>
				<ul class="bnrs-side">
					<li><a href="https://recruit.bee-music.jp/" target="_blank"><img src="/vocal/assets/img/index/bnr01.jpg" alt="講師採用情報 - BeeMusicSchool採用サイトへ"></a></li>
					<!-- <li><a href="/vocal/campaign/" target="_blank" onclick="window.open(this.href,'campaign','width=960,height=800,resizable=yes,scrollbars=yes');return false;"><img src="/vocal/assets/img/index/bnr03.jpg" alt="[無料]レッスン3回無料キャンペーン"></a></li> -->
					<li><a href="/vocal/sns_accounts/" target="_blank" onclick="window.open(this.href,'sns_accounts','width=960,height=800,resizable=yes,scrollbars=yes');return false;"><img src="/vocal/assets/img/index/bnr05.jpg" alt="beeミュージックスクールSNSアカウント一覧"></a></li>
				</ul>
			</div></aside>

			<main id="main" class="hajust-cts"><div id="main-inner">

				<h1 class="page-ttl line1"><strong>45分無料体験レッスン</strong></h1>

				<section class="section">
					<p>この度は、Beeボーカルスクールへのお問い合わせをいただきまして、誠にありがとうございます。ご記入頂きましたメールアドレス宛に入力していただいた内容を返送しておりますのでご確認ください。</p>
					<p>なお数日中に担当者より連絡がない場合、何らかの事情でお問い合わせ内容が正常に送信されなかった可能性があります。その際は大変お手数ですが、下記窓口までお問い合わせいただければ幸いです。</p>
					<dl class="mt20">
						<dt class="b">お問い合わせ窓口</dt>
						<dd>株式会社ビー・ファクトリー<br>〒171-0021　東京都豊島区西池袋5-4-7　池袋トーセイビル4F<br>TEL:03-6914-1814<br>月～金　9:30～21:30　土・日　9:30～19:30</dd>
					</dl>
				</section>

			</div></main>

		</article>
	</div>
</article>
<!-- △△△△△ contents △△△△△ -->

<?php require($_SERVER['DOCUMENT_ROOT'].'/vocal/include/footer.php'); ?>

</div><!-- //wrap -->

</body>
</html>
