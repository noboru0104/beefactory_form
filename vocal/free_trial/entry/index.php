<!doctype html>
<html>
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WQNZJ8P');</script>
<!-- End Google Tag Manager -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="keywords" content="ボイストレーニング,ボイトレ,ボーカルスクール">
<meta name="description" content="ボイストレーニングなら、Beeボーカルスクール！経験豊富なインストラクターによるマンツーマンのボイトレで、初心者からプロ志望まで対応！新宿、池袋、渋谷、赤羽の駅近でアクセス良好です。">
<meta name="author" content="株式会社ビー・ファクトリー">
<meta name="copyright" content="Copyright © Bee Factory, Inc. All rights reserved.">
<meta property="og:title" content="ボイストレーニングのBeeボーカルスクール">
<meta property="og:type" content="website">
<meta property="og:url" content="https://www.bee-music.jp/vocal/">
<meta property="og:image" content="https://www.bee-music.jp/vocal/assets/img/og-image.jpg">
<meta property="og:site_name" content="ボイストレーニングのBeeボーカルスクール">
<meta property="og:description" content="ボイストレーニングなら、Beeボーカルスクール！経験豊富なインストラクターによるマンツーマンのボイトレで、初心者からプロ志望まで対応！新宿、池袋、渋谷、赤羽の駅近でアクセス良好です。">
<meta name="twitter:card" content="summary">
<meta name="twitter:description" content="ボイストレーニングなら、Beeボーカルスクール！経験豊富なインストラクターによるマンツーマンのボイトレで、初心者からプロ志望まで対応！新宿、池袋、渋谷、赤羽の駅近でアクセス良好です。">
<title>無料体験レッスンのお申し込み入力｜ボイストレーニングのBeeボーカルスクール(新宿・池袋・渋谷)</title>
<!-- ▼共通CSS▼ -->
<link rel="stylesheet" type="text/css" href="../css/common/reset.css">
<!-- ▲共通CSS▲ -->
<!-- ▼▼個別CSS▼▼ -->
<link rel="stylesheet" type="text/css" href="../css/form.css">
<!-- ▼ドラムロール関連CSS▼ -->
<link rel="stylesheet" type="text/css" href="../css/common/mobiscroll.custom-2.6.2.min.css">
<link rel="stylesheet" type="text/css" href="../css/common/mobiscroll.css">
<!-- ▲ドラムロール関連CSS▲ -->
<!--スライダー入力-->
<link rel="stylesheet" href="../css/common/jquery.bxslider.css">
<!-- ▲▲個別CSS▲▲ -->
<!-- ▼favicon▼ -->
<link rel="shortcut icon" href="../images/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="icon" href="../images/favicon.ico" type="image/vnd.microsoft.icon">
<!-- ▲favicon▲ -->
<!-- ▼共通JS▼ -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<!-- ▲共通JS▲ -->

<!-- ▼▼個別JS▼▼ -->
<!--スライダー入力-->
<script type="text/javascript" src="../js/common/jquery.bxslider.js"></script>
<!-- ▼入力フォーム関連JS▼ -->
<script type="text/javascript" src="../js/common/jquery.matchHeight.js"></script>
<script type="text/javascript" src="../js/entry/entry.js"></script>
<!-- ▲入力フォーム関係JS▲ -->
<!-- ▼ドラムロール関連JS▼ -->

<script type="text/javascript" src="../js/common/mobiscroll.custom-2.6.2.min.js"></script>
<script type="text/javascript" src="../js/common/mobiscroll.js"></script>
<!-- ▲ドラムロール関連JS▲ -->
<!-- ▲▲個別JS▲▲ -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQNZJ8P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header>    
    <div class="l-header">
        <div class="l-header-table">
            <div class="l-header-left">
                <h1><a href="/"><img src="../images/common/btn_header_logo_sp.png" alt="Beeボーカルスクール"></a></h1>
            </div>
        </div>
    </div>
</header>
<div class="l-mvBlock">
    <div class="l-mv">
    	<div class="l-inner02">
            <p class="p-title">無料体験レッスンのお申し込み</p>
            <p class="p-image"><img src="../images/form/img_step01_sp.png" alt="入力"></p>
        </div>
    </div>
</div>
<div id="wrapper">
	<div id="contact" class="l-contactBlock">
    	<div>
    		<form method="post" action="../confirm/" id="entry">
            <div class="slider">
                <div class="l-table namae">
                    <div class="l-title">                    	
                        <div class="p-title"><p>基本情報</p></div>
                        <div class="p-number">
                            <p><img src="../images/form/img_number01_sp.png" alt="基本情報"></p>
                        </div>
                    </div>                  
                    <table>
                        <tr>
                            <th>お名前<span><img src="../images/form/img_hissu_sp.png" alt="必須"></span></th>
                            <td>
                                <input type="text" class="p-namae" id="namae" data-current="namae" name="namae" placeholder="例）テスト太郎">
                                <span class="p-err p-namae-err hid">※お名前は必須です</span>
                            </td>
                        </tr>
                        <tr>
                            <th>ご年代<span><img src="../images/form/img_hissu_sp.png" alt="必須"></span></th>
                            <td>
                                <select class="p-age" name="age">
                                    <option value="">選択してください</option>
                                    <option value="～９歳">～９歳</option>
                                    <option value="１０代">１０代</option>
                                    <option value="２０代">２０代</option>
                                    <option value="３０代">３０代</option>
                                    <option value="４０代">４０代</option>
                                    <option value="５０代">５０代</option>
                                    <option value="６０代以上">６０代以上</option>
                                </select>
                                <span class="p-err p-age-err hid">※ご年代は必須です</span>
                            </td>
                        </tr>
                    </table>
                    <div class="l-agree">
                    	<div class="l-agree-table">
                            <p class="p-message">
                                未成年の方は保護者の了承を得ている場合は下記項目にチェックして下さい。了承を得られていない場合はお受け付け出来かねます。予めご了承ください。
                            </p>
                            <div class="l-agreeBlock">
                            	<label>
                                    <input type="checkbox" name="checkbox01[]" class="checkbox01-input">
                                    <span class="checkbox01-parts">同意する</span>
                                </label>
                            </div>
                            <span class="p-err p-checkbox01-err hid">※同意は必須です</span>
                        </div>
                    </div>
                    <table>
                        <tr>
                            <th>電話番号<span><img src="../images/form/img_hissu_sp.png" alt="必須"></span></th>
                            <td>
                                <input type="tel" class="p-tel" id="tel" data-current="tel" name="tel" placeholder="例）08012345678" maxlength="15">
                                <span class="p-err p-tel-err hid">※電話番号は必須です</span>
                            </td>
                        </tr>
                        <tr>
                            <th>メールアドレス<span><img src="../images/form/img_hissu_sp.png" alt="必須"></span></th>
                            <td>
                                <input type="text" class="p-mail" id="mail" data-current="mail" name="mail" placeholder="例）sample@beemusic.jp">
                                <span class="p-err p-mail-err hid">※メールアドレスは必須です</span>
                            </td>
                        </tr>
                    </table>
                    <div class="l-button l-step01">
                        <p class="p-back">
                        	<a id="stepback01" href="javascript:history.back();">戻る</a>
                        </p><p class="p-next">
                        	<input id="stepnext01" type="button" class="js-next btn-namae js-step01" data-name="namae" data-number="2" value="次へ">                            
                        </p>
                    </div>
                </div>                
                <div class="l-table js-mailcheck03 area">                    
                    <div class="l-title">                    	
                        <div class="p-title"><p>レッスン情報</p></div>
                        <div class="p-number">
                            <p><img src="../images/form/img_number02_sp.png" alt="レッスン情報"></p>
                        </div>
                    </div>
                    <table>
                        <tr>
                            <th>希望エリア<span><img src="../images/form/img_hissu_sp.png" alt="必須"></span></th>
                            <td>
                            	<div class="l-area-table">
                                	<div class="l-area-header">
                                    	<label>
                                            <input type="radio" name="area" class="area-input p-area-input01" value="希望エリア" checked>
                                            <span class="area-parts">希望エリアを選択する（複数選択可）</span>
                                        </label>
                                    </div>
                                    <div class="l-area-detail">
                                    	<label>
                                            <input type="checkbox" name="checkbox03[]" class="checkbox03-input" value="新宿">
                                            <span class="checkbox03-parts">新宿</span>
                                        </label><label>
                                            <input type="checkbox" name="checkbox03[]" class="checkbox03-input" value="渋谷">
                                            <span class="checkbox03-parts">渋谷</span>
                                        </label><label>
                                            <input type="checkbox" name="checkbox03[]" class="checkbox03-input" value="池袋">
                                            <span class="checkbox03-parts">池袋</span>
                                        </label><label>
                                            <input type="checkbox" name="checkbox03[]" class="checkbox03-input" value="赤羽">
                                            <span class="checkbox03-parts">赤羽</span>
                                        </label>
                                    </div>
                                	<span class="p-err p-area-err hid">※希望エリアを選択してください</span>
                                </div>
                                <div class="l-area-table">
                                	<div class="l-area-header p-all">
                                    	<label>
                                            <input type="radio" name="area" class="area-input p-area-input02" value="相談して決める">                                            
                                            <span class="area-parts">相談して決める</span>
                                        </label>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="l-button l-step02">
                        <p class="p-back">
                        	<input id="stepback02" type="button" class="js-back" data-number="1" value="戻る">
                        </p><p class="p-next">
                        	<input id="stepnext02" type="button" class="js-next btn-furigana js-step02" data-name="furigana" data-number="3" value="次へ">
                        </p>
                    </div>
                </div>               
                <div class="l-table js-mailcheck04 kibou2">                    
                    <div class="l-title">                    	
                        <div class="p-title"><p>レッスン情報</p></div>
                        <div class="p-number">
                            <p><img src="../images/form/img_number03_sp.png" alt="レッスン情報"></p>
                        </div>
                    </div>
                    <table>
                        <tr>
                            <th>希望受講日時</th>
                            <td>
                            	<div class="l-area-table3">
                                	<div class="l-area-header3">
                                    	<label>
                                            <input type="radio" name="area3" class="area3-input p-area3-input01" value="日付を決める" checked>
                                            <span class="area3-parts">日付を決める</span>
                                        </label><label>
                                            <input type="radio" name="area3" class="area3-input p-area3-input02" value="相談して決める">
                                            <span class="area3-parts">相談して決める</span>
                                        </label>
                                    </div>
                                    <div class="l-area-detail3">
                                    	<div class="kibou1">
                                            <p class="p-title">第一希望<span><img src="../images/form/img_hissu_sp.png" alt="必須"></span></p>
                                            <input readonly type="text" class="p-kibou p-kibou1_1" id="kibou1_1" data-current="kibou1_1" name="kibou1_1" placeholder="タップして選択して下さい">
                                    	</div>
                                		<span class="p-err p-kibou1-err hid">※第一希望を入力してください</span>
                                    
                                    	<div class="kibou1">
                                            <p class="p-title">第二希望<span><img src="../images/form/img_hissu_sp.png" alt="必須"></span></p>
                                            <input readonly type="text" class="p-kibou p-kibou2_1" id="kibou2_1" data-current="kibou2_1" name="kibou2_1" placeholder="タップして選択して下さい">
                                    	</div>
                                		<span class="p-err p-kibou2-err hid">※第二希望を入力してください</span>
                                    
                                    	<div class="kibou1">
                                            <p class="p-title">第三希望<span><img src="../images/form/img_nini_sp.png" alt="任意"></span></p>
                                            <input readonly type="text" class="p-kibou p-kibou3_1" id="kibou3_1" data-current="kibou3_1" name="kibou3_1" placeholder="タップして選択して下さい">
                                    	</div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="l-button l-step03">
                        <p class="p-back">
                        	<input id="stepback03" type="button" class="js-back" data-number="2" value="戻る">
                        </p><p class="p-next">
                        	<input id="stepnext03" type="button" class="js-next btn-area js-step03" data-name="area" data-number="4" value="次へ">
                        </p>
                    </div>
                </div>               
                <div class="l-table js-mailcheck05 sonota">                    
                    <div class="l-title">                    	
                        <div class="p-title"><p>レッスン情報</p></div>
                        <div class="p-number">
                            <p><img src="../images/form/img_number04_sp.png" alt="レッスン情報"></p>
                        </div>
                    </div>  
                    <table class="l-step05-table">
                        <tr>
                            <th>ご質問・ご要望<span><img src="../images/form/img_nini_sp.png" alt="任意"></span></th>
                            <td>
                            	<p class="p-message">
                                	お好きな音楽のジャンル、希望する講師の特徴、その他の体験レッスン受講希望日・曜日・時間帯、レッスン受講の動機など、ご要望やご質問などがございましたら、お気軽にご記入ください。（300字以内）
                                </p>
                                <p class="p-caution">
                                	※状況によって、ご期待に添いかねるケースがございますが、最大限対応させて頂きます。
                                </p>
                                <p>
                                	<textarea id="other" class="p-other" name="other" rows="8" maxlength="300" placeholder="質問や要望などございましたら、気軽にご記入、ご相談下さい。&#13;&#10;&#13;&#10;例）「優しい先生を希望します」「スマホでDJがやりたい」「友人の結婚式でDJをやりたい」"></textarea>
                                </p>
                            </td>
                        </tr>
                    </table>
                    <div class="l-button l-step04">
                        <p class="p-back">
                        	<input id="stepback05" type="button" class="js-back" data-number="3" value="戻る">
                        </p><p class="p-next">
                        	<input id="stepnext05" type="submit" class="js-next" value="確認画面へ進む">
                        </p>
                    </div>
                </div>
            </div>
            </form>
    	</div>       
    </div>
</div>
<div class="modal">
    <div class="modal-table">
        <div class="modal-row">
            <div class="modal-cell">
                <div>
                    <div class="p-date">
                        <div>
                            
                        </div>
                    </div>
                    <div class="p-clock"></div>
                    <div class="p-button">
                        <div>
                            <p>
                                <input type="button" class="js-cancel" style="display:none;" value="キャンセル">
                            </p><p>
                                <input type="button" class="js-ok" value="決定">
                            </p>
                        </div>
                    </div>
                    <div class="p-close">
                    	<img src="../images/form/img_close_sp.png" class="js-close" alt="close">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-back"></div>
</div>
<footer>
    <div>
        <p class="p-message">ボイストレーニング・ボイトレの<br>Beeボーカルスクール (新宿・渋谷・池袋・赤羽)</p>
        <p class="p-copy">Copyright &copy; 2009-2015 Bee Vocal School.<br>All Rights Reserved. </p>
    </div>
</footer>
</body>
</html>
