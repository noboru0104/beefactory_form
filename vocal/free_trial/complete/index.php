<?php
session_start();
header("Content-type: text/html; charset=UTF-8");
mb_language("Japanese");
mb_internal_encoding("UTF-8");
//mb_detect_order("ASCII, JIS, UTF-8, EUC-JP, SJIS");
error_reporting(E_ALL ^ E_NOTICE);

if($_SESSION['area'] === '希望エリア'){
	$area = $_SESSION['kibou_area'];
} else {
	$area = $_SESSION['area'];
}

if($_SESSION['jyukou'] === '日付を決める'){
	$kibou = "\n・" .$_SESSION['kibou1_1']."\n・".$_SESSION['kibou2_1']."\n・".$_SESSION['kibou3_1'];
} else {
	$kibou = $_SESSION['jyukou'];
}

$to_name = $_SESSION['namae'] ." 様";
$to_addr = $_SESSION['mail'];
$to_name_enc = mb_encode_mimeheader($to_name, "ISO-2022-JP");
$to = "$to_name_enc <$to_addr>";
$re_from = $to;

$adress = "web@bee-music.jp";

$from_name = "Beeボーカルスクール";
$from_addr = $adress;//会社側
$reply_name = "Beeボーカルスクール";
$reply_addr = $adress;//会社側

$from_name_enc = mb_encode_mimeheader($from_name, "ISO-2022-JP");
$from = "$from_name_enc <$from_addr>";
$re_to = $from;

$reply_name_enc = mb_encode_mimeheader($reply_name, "ISO-2022-JP");
$reply = "$reply_name_enc <$reply_addr>";

$header  = "MIME-Version: 1.0\r\n";
$header .= "Content-Transfer-Encoding: 7bit\r\n";
$header .= "Content-Type : text/plain;\r\n";
$header .= "\tcharset=\"iso-2022-jp\";\r\n";
$re_header = $header;
$header .= "From: $from\r\n";
$header .= "Reply-To: $reply\r\n";

$ua = $_SERVER['HTTP_USER_AGENT'];
$kisyu = "";

if ((strpos($ua, 'Android') !== false) && (strpos($ua, 'Mobile') !== false) || (strpos($ua, 'iPhone') !== false) || (strpos($ua, 'Windows Phone') !== false)) {
    // スマートフォンからアクセスされた場合
    if ((strpos($ua, 'Android') !== false) && (strpos($ua, 'Mobile') !== false)) {
		$kisyu = "スマートフォンAndroid";
	} elseif (strpos($ua, 'iPhone') !== false) {
		$kisyu = "スマートフォンiOS";
	} elseif (strpos($ua, 'Windows Phone') !== false) {
		$kisyu = "スマートフォンWindows Phone";
	} else {
		$kisyu = "スマートフォンその他";
	}	
    //exit();

} elseif ((strpos($ua, 'Android') !== false) || (strpos($ua, 'iPad') !== false)) {
    // タブレットからアクセスされた場合
    //$kisyu = "タブレット";
	if (strpos($ua, 'Android') !== false) {
		$kisyu = "タブレットAndroid";
	} elseif (strpos($ua, 'iPad') !== false) {
		$kisyu = "タブレットiPad";
	} else {
		$kisyu = "タブレットその他";
	}
    //exit();

} elseif ((strpos($ua, 'DoCoMo') !== false) || (strpos($ua, 'KDDI') !== false) || (strpos($ua, 'SoftBank') !== false) || (strpos($ua, 'Vodafone') !== false) || (strpos($ua, 'J-PHONE') !== false)) {
    // 携帯からアクセスされた場合
    //$kisyu = "携帯電話";
	if (strpos($ua, 'DoCoMo') !== false) {
		$kisyu = "携帯電話\nDoCoMo";
	} elseif (strpos($ua, 'KDDI') !== false) {
		$kisyu = "携帯電話\nKDDI";
	} elseif (strpos($ua, 'SoftBank') !== false) {
		$kisyu = "携帯電話\nSoftBank";
	} elseif (strpos($ua, 'Vodafone') !== false) {
		$kisyu = "携帯電話\nVodafone";
	} elseif (strpos($ua, 'J-PHONE') !== false) {
		$kisyu = "携帯電話\nJ-PHONE";
	} else {
		$kisyu = "携帯電話\nその他";
	}
    //exit();

} elseif ((strpos($ua, 'Windows') !== false) || (strpos($ua, 'Macintosh') !== false)) {
    // その他（PC）からアクセスされた場合
    if (strpos($ua, 'Windows') !== false) {
		$kisyu = "Windows";
	} elseif (strpos($ua, 'Mac') !== false) {
		$kisyu = "Mac";
	} else {
		$kisyu = "パソコンその他";
	}
    //exit();
}

$subject =  "Beeミュージックスクールへのお問合せ受付のお知らせ（自動送信）";

$bodyy .= $_SESSION['namae'] ." 様"."\n";
$body_text = <<< EOF

この度は、Beeミュージックスクールへのお問い合わせをいただきまして、
誠にありがとうございます。

お問合わせ内容を確認させていただき、ご回答させていただきます。
なお、お問合わせの内容によってはご回答まで数日かかる場合がございます。
恐れ入りますが、ご連絡まで今しばらくお待ちいただきますよう、お願いいたします。

EOF;

$body .= "\n";
$body .= "================================"."\n";
$body .= "■ 無料体験レッスン希望について"."\n";
$body .= "【希望エリア】：" .$area."\n";
$body .= "【受講希望日時】：" .$kibou."\n\n\n";


$body .= "■ お客様情報"."\n";
$body .= "【お名前】：" .$_SESSION['namae'] ." 様"."\n";
$body .= "【年齢】：" .$_SESSION['age']."\n";
$body .= "【メールアドレス】：" .$_SESSION['mail']."\n";
$body .= "【電話番号】：" .$_SESSION['tel']."\n";
$body .= "【ご希望欄】：\n" .$_SESSION['other']."\n\n";



$re_body .= "ボーカルスクールより以下のとおり、問い合わせがありました。"."\n"."お問い合わせ内容をご確認ください。\n".$body;

$re_body .= "\n■ 補足情報"."\n";
$re_body .= "【未成年の保護者同意確認】：" .$_POST['agree']."\n";
$re_body .= "【IPアドレス】：" .$_SERVER["REMOTE_ADDR"] ."\n";
$re_body .= "【デバイス】：" .$kisyu;

$body = $bodyy.$body_text.$body;
$body .= <<< EOF

/*----------------------------------------------------------------------------------------*
株式会社ビー・ファクトリー
〒171-0021　東京都豊島区西池袋5-4-7　池袋トーセイビル4F
TEL:03-6914-1815（月～金 11：00～22：00 土・日　11:00～20:00）
https://www.bee-music.jp/
*----------------------------------------------------------------------------------------*/

EOF;

if($_SESSION['namae']){
	mb_send_mail($to, $subject, $body, $header,"-f$from_addr");
}

$re_header .= "From: $re_from\r\n";
$re_header .= "Reply-To: $re_from\r\n";
$re_subject = "ボーカルスクール｜無体レッスン";
if($_SESSION['namae']){
	$re_to = $adress;//会社側
	mb_send_mail($re_to, $re_subject, $re_body, $re_header,"-f$to_addr");
}

$_SESSION = array();
setcookie( session_name(), '', time()-60);
session_destroy();
?>
<!doctype html>
<html>
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WQNZJ8P');</script>
<!-- End Google Tag Manager -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="keywords" content="ボイストレーニング,ボイトレ,ボーカルスクール">
<meta name="description" content="ボイストレーニングなら、Beeボーカルスクール！経験豊富なインストラクターによるマンツーマンのボイトレで、初心者からプロ志望まで対応！新宿、池袋、渋谷、赤羽の駅近でアクセス良好です。">
<meta name="author" content="株式会社ビー・ファクトリー">
<meta name="copyright" content="Copyright © Bee Factory, Inc. All rights reserved.">
<meta property="og:title" content="ボイストレーニングのBeeボーカルスクール">
<meta property="og:type" content="website">
<meta property="og:url" content="https://www.bee-music.jp/vocal/">
<meta property="og:image" content="https://www.bee-music.jp/vocal/assets/img/og-image.jpg">
<meta property="og:site_name" content="ボイストレーニングのBeeボーカルスクール">
<meta property="og:description" content="ボイストレーニングなら、Beeボーカルスクール！経験豊富なインストラクターによるマンツーマンのボイトレで、初心者からプロ志望まで対応！新宿、池袋、渋谷、赤羽の駅近でアクセス良好です。">
<meta name="twitter:card" content="summary">
<meta name="twitter:description" content="ボイストレーニングなら、Beeボーカルスクール！経験豊富なインストラクターによるマンツーマンのボイトレで、初心者からプロ志望まで対応！新宿、池袋、渋谷、赤羽の駅近でアクセス良好です。">
<title>無料体験レッスンのお申し込み完了｜ボイストレーニングのBeeボーカルスクール(新宿・池袋・渋谷)</title>
<!-- ▼共通CSS▼ -->
<link rel="stylesheet" type="text/css" href="../css/common/reset.css">
<!-- ▲共通CSS▲ -->
<!-- ▼▼個別CSS▼▼ -->
<link rel="stylesheet" type="text/css" href="../css/complete.css">
<!-- ▲▲個別CSS▲▲ -->
<!-- ▼favicon▼ -->
<link rel="shortcut icon" href="../images/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="icon" href="../images/favicon.ico" type="image/vnd.microsoft.icon">
<!-- ▲favicon▲ -->
<!-- ▼共通JS▼ -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="../js/complete/complete.js"></script>
<!-- ▲共通JS▲ -->

<!-- ▼▼個別JS▼▼ -->
<!-- ▲▲個別JS▲▲ -->

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQNZJ8P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header>    
    <div class="l-header">
        <div class="l-header-table">
            <div class="l-header-left">
                <h1><a href="/"><img src="../images/common/btn_header_logo_sp.png" alt="Beeボーカルスクール"></a></h1>
            </div>
        </div>
    </div>
</header>
<div class="l-mvBlock">
    <div class="l-mv">
    	<div class="l-inner02">
            <p class="p-title">無料体験レッスンのお申し込み</p>
            <p class="p-image"><img src="../images/form/img_step03_sp.png" alt="完了"></p>
        </div>
    </div>
</div>
<div id="wrapper">
	<div id="contact" class="l-contactBlock">
    	<div>
    		<div class="slider">
                <div class="l-table namae">                      
                    <table class="l-step05-table">
                        <tr>
                            <td>
                            	<p class="p-title">
                                	無料体験レッスンを受付いたしました。 
                                </p>
                                <p class="p-message">
                                	お申し込みいただきありがとうございました。ご記入いただいた内容は、自動返信メールをご確認ください。後ほど担当者よりお電話にてご連絡いたします。
                                </p>
                                <p class="p-title">
                                	万が一の場合
                                </p>
                                <p class="p-message">
                                	自動返信メールが届かない、日程を変更したいなどのご相談や、その他ご不明点などございましたら下記の電話番号までお問い合わせください。
                                </p>
                                <p class="p-button">
                                    <a href="tel:0120-015-349"><img src="../images/contact/btn_tel_sp.png" alt="0120-015-349 月～金 9:30～21:30 土日 9:30～19:30 タップで発信！"></a>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
    	</div>    
    </div>
</div>
<footer>
    <div>
        <p class="p-message">ボイストレーニング・ボイトレの<br>Beeボーカルスクール (新宿・渋谷・池袋・赤羽)</p>
        <p class="p-copy">Copyright &copy; 2009-2015 Bee Vocal School.<br>All Rights Reserved. </p>
    </div>
</footer>
</body>
</html>
