<?php
session_start();
header("Content-type: text/html; charset=UTF-8");
mb_language("Japanese");
mb_internal_encoding("UTF-8");
//mb_detect_order("ASCII, JIS, UTF-8, EUC-JP, SJIS");
error_reporting(E_ALL ^ E_NOTICE);

$to_name = "離脱メール";
$to_addr = $_POST['mail'];
$to_name_enc = mb_encode_mimeheader($to_name, "ISO-2022-JP");
$to = "$to_name_enc <$to_addr>";
$re_from = $to;

$adress = "web@bee-music.jp";

$from_name = "Beeボーカルスクール";
$from_addr = $adress;//会社側
$reply_name = "Beeボーカルスクール";
$reply_addr = $adress;//会社側

$from_name_enc = mb_encode_mimeheader($from_name, "ISO-2022-JP");
$from = "$from_name_enc <$from_addr>";
$re_to = $from;

$reply_name_enc = mb_encode_mimeheader($reply_name, "ISO-2022-JP");
$reply = "$reply_name_enc <$reply_addr>";

$header  = "MIME-Version: 1.0\r\n";
$header .= "Content-Transfer-Encoding: 7bit\r\n";
$header .= "Content-Type : text/plain;\r\n";
$header .= "\tcharset=\"iso-2022-jp\";\r\n";
$re_header = $header;
$header .= "From: $from\r\n";
$header .= "Reply-To: $reply\r\n";

$ua = $_SERVER['HTTP_USER_AGENT'];
$kisyu = "";

if ((strpos($ua, 'Android') !== false) && (strpos($ua, 'Mobile') !== false) || (strpos($ua, 'iPhone') !== false) || (strpos($ua, 'Windows Phone') !== false)) {
    // スマートフォンからアクセスされた場合
    if ((strpos($ua, 'Android') !== false) && (strpos($ua, 'Mobile') !== false)) {
		$kisyu = "スマートフォンAndroid";
	} elseif (strpos($ua, 'iPhone') !== false) {
		$kisyu = "スマートフォンiOS";
	} elseif (strpos($ua, 'Windows Phone') !== false) {
		$kisyu = "スマートフォンWindows Phone";
	} else {
		$kisyu = "スマートフォンその他";
	}	
    //exit();

} elseif ((strpos($ua, 'Android') !== false) || (strpos($ua, 'iPad') !== false)) {
    // タブレットからアクセスされた場合
    //$kisyu = "タブレット";
	if (strpos($ua, 'Android') !== false) {
		$kisyu = "タブレットAndroid";
	} elseif (strpos($ua, 'iPad') !== false) {
		$kisyu = "タブレットiPad";
	} else {
		$kisyu = "タブレットその他";
	}
    //exit();

} elseif ((strpos($ua, 'DoCoMo') !== false) || (strpos($ua, 'KDDI') !== false) || (strpos($ua, 'SoftBank') !== false) || (strpos($ua, 'Vodafone') !== false) || (strpos($ua, 'J-PHONE') !== false)) {
    // 携帯からアクセスされた場合
    //$kisyu = "携帯電話";
	if (strpos($ua, 'DoCoMo') !== false) {
		$kisyu = "携帯電話\nDoCoMo";
	} elseif (strpos($ua, 'KDDI') !== false) {
		$kisyu = "携帯電話\nKDDI";
	} elseif (strpos($ua, 'SoftBank') !== false) {
		$kisyu = "携帯電話\nSoftBank";
	} elseif (strpos($ua, 'Vodafone') !== false) {
		$kisyu = "携帯電話\nVodafone";
	} elseif (strpos($ua, 'J-PHONE') !== false) {
		$kisyu = "携帯電話\nJ-PHONE";
	} else {
		$kisyu = "携帯電話\nその他";
	}
    //exit();

} elseif ((strpos($ua, 'Windows') !== false) || (strpos($ua, 'Macintosh') !== false)) {
    // その他（PC）からアクセスされた場合
    if (strpos($ua, 'Windows') !== false) {
		$kisyu = "Windows";
	} elseif (strpos($ua, 'Mac') !== false) {
		$kisyu = "Mac";
	} else {
		$kisyu = "パソコンその他";
	}
    //exit();
}

$subject =  "【離脱】ボーカルスクール｜無体レッスン";

$body_text = <<< EOF

下記お客様がボーカルスクールの無体レッスンフォームから離脱されました。
内容をご確認ください。
EOF;

$body .= "\n\n";
$body .= "================================"."\n";
$body .= "■ お客様情報"."\n";
$body .= "【お名前】：" .$_POST['namae'] ." 様"."\n";
$body .= "【年齢】：" .$_POST['age']."\n";
$body .= "【メールアドレス】：" .$_POST['mail']."\n";
$body .= "【電話番号】：" .$_POST['tel']."\n";
//$body .= "【希望エリア】：" .$area."\n";
//$body .= "【受講希望日時】：" .$kibou."\n";
$body .= "【ご希望欄】：\n" .$_POST['other']."\n\n";
$body .= "■ 補足情報"."\n";
$body .= "【未成年の保護者同意確認】：" .$_POST['agree']."\n";
$body .= "【IPアドレス】：" .$_SERVER["REMOTE_ADDR"] ."\n";
$body .= "【デバイス】：" .$kisyu;

$body = $body_text.$body;

$re_header .= "From: $re_from\r\n";
$re_header .= "Reply-To: $re_from\r\n";
$re_subject = "【離脱】ボーカルスクール｜無体レッスン";

$re_body = $body;

$re_to = $adress;//会社側
mb_send_mail($re_to, $re_subject, $re_body, $re_header,"-f$from_addr");


$_SESSION = array();
setcookie( session_name(), '', time()-60);
session_destroy();
?>
