<!DOCTYPE html>
<html lang="ja" xmlns="https://www.w3.org/1999/xhtml" xmlns:og="https://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
<?php require($_SERVER['DOCUMENT_ROOT'].'/vocal/include/analytics_head_start.php'); ?>

<title>45分無料体験レッスン | ボイストレーニングのBeeボーカルスクール(新宿・池袋・渋谷)</title>

<?php require($_SERVER['DOCUMENT_ROOT'].'/vocal/include/head.php'); ?>
<style>
	#aside #aside-inner ul.bnrs-side{
		margin-top: 0;
	}
	#fix-contact-pc .container .inq{
		width: 510px;
		margin: 0 auto;
		float: none;
	}
	#fix-contact-pc .container .inq dl dd.tel{
		text-align: center;
	}
	#fix-contact-sp ul li:first-of-type{
		width: 63%;
		color: #fff;
		line-height: 1.25;
	}
	#fix-contact-sp ul li:first-of-type a{
		display: block;
		margin-top: 2px;
	}
	#fix-contact-sp ul li:last-of-type{
		margin-top: 17px;
		float: right;
	}
</style>
<script>
jQuery(function($){
	$('#formSubmit').taskSubmit('send','form[name="form"]');
	$('#formBack').taskSubmit('back','form[name="form"]');
});
</script>
</head>
<body id="top" class="vocal free-trial">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQNZJ8P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="wrap">

<!-- ▽▽▽▽▽ header ▽▽▽▽▽ -->
<header id="header">
	<div class="lead">
		<div class="container">
			<p>歌がもっと上手くなりたいあなたのためのBeeボーカルスクール。新宿・渋谷・池袋・赤羽の駅から駅近＆キレイで、お子様や女性にも安心して通って頂けるボイストレーニング教室です。</p>
			<ul class="sns">
				<li><a href="https://www.youtube.com/channel/UCWglVipGXNMtXeHvv7Cv9GQ" target="_blank"><img src="/vocal/assets/img/parts/h-sns-youtube.png" alt="youtube"></a></li>
				<li><a href="https://www.facebook.com/beemusic.jp/" target="_blank"><img src="/vocal/assets/img/parts/h-sns-fb.png" alt="facebook"></a></li>
			</ul>
		</div>
	</div>
	<div class="desc">
		<div class="container">
			<div class="ttls">
				<p class="l-logo"><a href="/vocal/"><img src="/vocal/assets/img/parts/h-site-logo-vocal.png" alt="Beeボーカルスクール[Bee vocal school]" /></a></p>
			</div>
			<div class="inq">
				<dl>
					<dt>お電話による無料体験レッスンお申込み、お問い合わせはこちら</dt>
					<dd class="op">受付時間：<br>平日 9:30-21:30<br>土日 9:30-19:30</dd>
					<dd class="tel"><img src="/vocal/assets/img/parts/h-contact-tel.png" alt="[フリーダイヤル]0120-015-349(イコーミュージック)携帯電話からも通話可能"></dd>
				</dl>
			</div>
			<a href="" id="nav-main-btn" data-tg="#nav-main"><span></span><span></span><span></span></a>
		</div>
	</div>

	<!-- <p class="header-news"><span>お知らせ：</span>12月29日(金)～1月3日(水)は休業とさせていただきます。ご不便をお掛けしますが、何卒よろしくお願い申し上げます。</p> -->

	<nav class="nav-menu" id="nav-main">
		<ul class="container">
			<li class="nav-home"><a href="/vocal/"><span>ホーム</span></a></li>
			<li><a href="/vocal/about/"><span>当スクールについて</span></a></li>
			<li><a href="/vocal/cource/"><span>料金/コース</span></a></li>
			<li><a href="/vocal/staff/"><span>スタッフ紹介</span></a></li>
			<li><a href="/vocal/voice/"><span>生徒さんの声</span></a></li>
			<li><a href="/vocal/faq/"><span>よくある質問</span></a></li>
			<li><a href="/vocal/access/"><span>アクセスマップ</span></a></li>
			<li class="nav-site">
				<ul>
					<li><a href="/vocal/company/">会社概要</a></li>
					<li><a href="/vocal/privacy/">プライバシーポリシー</a></li>
					<li><a href="/vocal/ad/">取材・広告へのお問い合わせ</a></li>
					<li><a href="/vocal/sitemap/">サイトマップ</a></li>
				</ul>
			</li>
			<li class="nav-bee">
				<ul>
					<li><a href="/vocal/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-vocal.png" alt="Beeボーカルスクール"><span><span class="ico-arrow"></span>Beeボーカルスクール</span></a></li>
					<li><a href="/piano/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-piano.png" alt="Beeピアノスクール"><span><span class="ico-arrow"></span>Beeピアノスクール</span></a></li>
					<li><a href="/guitar/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-guiter.png" alt="Beeギタースクール"><span><span class="ico-arrow"></span>Beeギタースクール</span></a></li>
					<li><a href="/voice/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-voice.png" alt="Beeボイススクール"><span><span class="ico-arrow"></span>Beeボイススクール</span></a></li>
					<li><a href="/lp/dj/standard/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-dj.png" alt="BeeDJスクール"><span><span class="ico-arrow"></span>BeeDJスクール</span></a></li>
					<li><a href="/lp/ukulele/standard/" target="_blank"><img src="/vocal/assets/img/parts/nav-bee-ukulele.png" alt="Beeウクレレスクール"><span><span class="ico-arrow"></span>Beeウクレレスクール</span></a></li>
				</ul>
			</li>
		</ul>
	</nav>
</header>
<!-- △△△△△ header △△△△△ -->

<!-- ▽▽▽▽▽ contents ▽▽▽▽▽ -->
<article id="contents">
	<div class="container">

		<article id="wrapper">

			<aside id="aside" class="hajust-cts"><div id="aside-inner">
				<ul class="bnrs-side">
					<li><a href="https://recruit.bee-music.jp/" target="_blank"><img src="/vocal/assets/img/index/bnr01.jpg" alt="講師採用情報 - BeeMusicSchool採用サイトへ"></a></li>
					<!-- <li><a href="/vocal/campaign/" target="_blank" onclick="window.open(this.href,'campaign','width=960,height=800,resizable=yes,scrollbars=yes');return false;"><img src="/vocal/assets/img/index/bnr03.jpg" alt="[無料]レッスン3回無料キャンペーン"></a></li> -->
					<li><a href="/vocal/sns_accounts/" target="_blank" onclick="window.open(this.href,'sns_accounts','width=960,height=800,resizable=yes,scrollbars=yes');return false;"><img src="/vocal/assets/img/index/bnr05.jpg" alt="beeミュージックスクールSNSアカウント一覧"></a></li>
				</ul>
			</div></aside>

			<main id="main" class="hajust-cts"><div id="main-inner">

				<h1 class="page-ttl line1"><strong>45分無料体験レッスン</strong></h1>

				<section class="section">
					<form name="form" method="post" action="<?php echo (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>#freeTrialForm">
						<h3 class="midashi3">無料レッスン希望スクールについて</h3>
						<dl class="form contact">
							<dt>希望エリア</dt>
							<dd>
							<?php
								$val2 = "";
								foreach ($vars['cf-school_flag-str'] as $key => $val) {
									if ($val === end($vars['cf-school_flag-str'])) {
										// 最後
										$val2 .= $val;
									} else {
										$val2 .= $val.',';
									}
								}
								if(strpos($val,'まだ決まっていないので相談したい') !== false){
									$val2 = 'まだ決まっていないので相談したい';
									$vars['cf-school-str'] = $val2;
									echo $val2;
								} else {
									$vars['cf-school-str'] = $val2;
									echo $val2;
								}
							?>
							</dd>
							<dt>希望受講日時</dt>
							<dd><?php if(isset($vars['cf-date_flag'])){
								if($vars['cf-date_flag']=='2'){
									echo $vars['cf-date_flag-str'];
								}else{
									if($vars['cf-time1-2'] == '' || $vars['cf-time1-2'] == '1'){
										$cf_time1_2 = '';
									} else {
										$cf_time1_2 = '～'.($vars['cf-time1-2-str']);
									}
									if($vars['cf-time2-2'] == '' || $vars['cf-time2-2'] == '1'){
										$cf_time2_2 = '';
									} else {
										$cf_time2_2 = '～'.$vars['cf-time2-2-str'];
									}

									if($vars['cf-time3-str'] == '選択してください'){
										$cf_time3_2 = '';
									} else {
										$cf_time3_2 = $vars['cf-time3-str'];
									}
									if($vars['cf-time3-2'] == '' || $vars['cf-time3-2'] == '1'){
										$cf_time3_2 .= '';
									} else {
										$cf_time3_2 .= '～'.$vars['cf-time3-2-str'];
									}
									echo '第一希望　'.$vars['cf-date'].' '.$vars['cf-time-str'].$cf_time1_2 .'<br>第二希望　'. $vars['cf-date2'].' '.$vars['cf-time2-str'].$cf_time2_2 .'<br>第三希望　'. $vars['cf-date3'].' '.$cf_time3_2;
								}
							} ?></dd>
						</dl>
						<h3 class="midashi3 mt20">お客様情報</h3>
						<dl class="form contact">
							<dt>お名前</dt>
							<dd><?php echo isset($vars['cf-name']) ? $vars['cf-name'] : ''; ?></dd>
							<dt>ご年代</dt>
							<dd><?php echo isset($vars['cf-age-str']) ? $vars['cf-age-str'] : ''; ?></dd>
							<dt>お電話番号</dt>
							<dd><?php echo isset($vars['cf-tel']) ? $vars['cf-tel'] : ''; ?></dd>
							<dt>メールアドレス</dt>
							<dd><?php echo isset($vars['cf-mail']) ? $vars['cf-mail'] : ''; ?></dd>
							<dt>ご希望欄</dt>
							<dd><?php echo isset($vars['cf-mess']) ? nl2br($vars['cf-mess']) : ''; ?></dd>
						</dl>

						<div class="button">
							<input type="button" class="back" value="　　　　戻る　　　" id="formBack">
							<input type="button" value="　　送信する　　" id="formSubmit">
						</div>

						<?php echo $_hiddens; ?>
						<input type="hidden" name="_task" value="">
						<input type="hidden" name="_csrf" value="<?php echo $csrf_token; ?>">
					</form>
				</section>

			</div></main>

		</article>
	</div>
</article>
<!-- △△△△△ contents △△△△△ -->

<!-- ▽▽▽▽▽ footer ▽▽▽▽▽ -->
<footer id="footer">
	<section class="infos">
		<div class="container">
			<dl class="info">
				<dt>ボイストレーニング・ボイトレの<br class="br-sp">Beeボーカルスクール（新宿・渋谷・池袋・赤羽）</dt>
				<dd class="copyright"><small>Copyright &copy; 2009-2015 Bee Vocal School.<br class="br-sp"> All Rights Reserved.</small></dd>
			</dl>
		</div>
	</section>
</footer>
<!-- △△△△△ footer △△△△△ -->

<article id="fix-contact-pc">
	<div class="container">
		<div class="inq">
			<dl>
				<dt>お電話による無料体験レッスンお申込み、お問い合わせはこちら</dt>
				<dd class="op"><span style="display: inline-block; float: left;">受付時間：</span>
				<span style="display: block; text-align: right; padding: 0 2em 0 0;">平日 9:30-21:30</span>
				<span style="display: block; text-align: right; padding: 0 2em 0 0;">土日 9:30-19:30</span></dd>
				<dd class="tel"><img src="/vocal/assets/img/parts/inq-tel-f.png" alt="[フリーダイヤル]0120-015-349(イコーミュージック)携帯電話からも通話可能"></dd>
			</dl>
		</div>
	</div>
</article>

<article id="fix-contact-sp">
	<ul>
		<li>受付時間：<br>平日 9:30-21:30　土日 9:30-19:30
			<a href="tel:0120-015-349"><img src="/vocal/assets/img/parts/inq-tel-f.png" alt="[フリーダイヤル]0120-015-349(イコーミュージック)携帯電話からも通話可能"></a></li>
		<li><a href="tel:0120-015-349" class="btn-def-white"><span>電話での<br>お問い合わせ</span></a></li>
	</ul>
</article>

</div><!-- //wrap -->

</body>
</html>