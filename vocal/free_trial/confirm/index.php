<?php
session_start();
header("Content-type: text/html; charset=UTF-8");
mb_language("Japanese");
mb_internal_encoding("UTF-8");
error_reporting(E_ALL ^ E_NOTICE);

/*必須*/
$namae = $_POST['namae'];
$age = $_POST['age'];
$tel = $_POST['tel'];
$mail = $_POST['mail'];

$area = $_POST['area'];
$kibou_area = '';
if($area === '希望エリア'){
	if (isset($_POST['checkbox03']) && is_array($_POST['checkbox03'])) {
		$kibou_area = implode(",", $_POST["checkbox03"]);
	}
} else {
	$kibou_area = '';
}

$jyukou = $_POST['area3'];
$kibou1_1 = '';
$kibou2_1 = '';
$kibou3_1 = '';
if($jyukou === '日付を決める'){
	$kibou1_1 = $_POST['kibou1_1'];
	$kibou2_1 = $_POST['kibou2_1'];
	$kibou3_1 = $_POST['kibou3_1'];
} else {
	$kibou1_1 = '';
	$kibou2_1 = '';
	$kibou3_1 = '';
}

/*任意*/
$other = $_POST['other'];

if(isset($namae,$age,$tel,$mail,$area,$kibou_area,$jyukou,$kibou1_1,$kibou2_1,$kibou3_1,$other)){
    $_SESSION['namae'] = htmlentities($namae, ENT_QUOTES, "UTF-8");
	$_SESSION['age'] = htmlentities($age, ENT_QUOTES, "UTF-8");
	$_SESSION['tel'] = htmlentities($tel, ENT_QUOTES, "UTF-8");
	$_SESSION['mail'] = htmlentities($mail, ENT_QUOTES, "UTF-8");	
	$_SESSION['area'] = htmlentities($area, ENT_QUOTES, "UTF-8");
	$_SESSION['kibou_area'] = htmlentities($kibou_area, ENT_QUOTES, "UTF-8");	
	$_SESSION['jyukou'] = htmlentities($jyukou, ENT_QUOTES, "UTF-8");
	$_SESSION['kibou1_1'] = htmlentities($kibou1_1, ENT_QUOTES, "UTF-8");
	$_SESSION['kibou2_1'] = htmlentities($kibou2_1, ENT_QUOTES, "UTF-8");
	$_SESSION['kibou3_1'] = htmlentities($kibou3_1, ENT_QUOTES, "UTF-8");
	$_SESSION['other'] = htmlentities($other, ENT_QUOTES, "UTF-8");
}

?>
<!doctype html>
<html>
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WQNZJ8P');</script>
<!-- End Google Tag Manager -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="keywords" content="ボイストレーニング,ボイトレ,ボーカルスクール">
<meta name="description" content="ボイストレーニングなら、Beeボーカルスクール！経験豊富なインストラクターによるマンツーマンのボイトレで、初心者からプロ志望まで対応！新宿、池袋、渋谷、赤羽の駅近でアクセス良好です。">
<meta name="author" content="株式会社ビー・ファクトリー">
<meta name="copyright" content="Copyright © Bee Factory, Inc. All rights reserved.">
<meta property="og:title" content="ボイストレーニングのBeeボーカルスクール">
<meta property="og:type" content="website">
<meta property="og:url" content="https://www.bee-music.jp/vocal/">
<meta property="og:image" content="https://www.bee-music.jp/vocal/assets/img/og-image.jpg">
<meta property="og:site_name" content="ボイストレーニングのBeeボーカルスクール">
<meta property="og:description" content="ボイストレーニングなら、Beeボーカルスクール！経験豊富なインストラクターによるマンツーマンのボイトレで、初心者からプロ志望まで対応！新宿、池袋、渋谷、赤羽の駅近でアクセス良好です。">
<meta name="twitter:card" content="summary">
<meta name="twitter:description" content="ボイストレーニングなら、Beeボーカルスクール！経験豊富なインストラクターによるマンツーマンのボイトレで、初心者からプロ志望まで対応！新宿、池袋、渋谷、赤羽の駅近でアクセス良好です。">
<title>無料体験レッスンのお申し込み確認｜ボイストレーニングのBeeボーカルスクール(新宿・池袋・渋谷)</title>
<!-- ▼共通CSS▼ -->
<link rel="stylesheet" type="text/css" href="../css/common/reset.css">
<!-- ▲共通CSS▲ -->
<!-- ▼▼個別CSS▼▼ -->
<link rel="stylesheet" type="text/css" href="../css/confirm.css">
<!-- ▲▲個別CSS▲▲ -->
<!-- ▼favicon▼ -->
<link rel="shortcut icon" href="../images/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="icon" href="../images/favicon.ico" type="image/vnd.microsoft.icon">
<!-- ▲favicon▲ -->
<!-- ▼共通JS▼ -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="../js/confirm/confirm.js"></script>
<!-- ▲共通JS▲ -->

<!-- ▼▼個別JS▼▼ -->
<!-- ▲▲個別JS▲▲ -->

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQNZJ8P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header>    
    <div class="l-header">
        <div class="l-header-table">
            <div class="l-header-left">
                <h1><a href="/"><img src="../images/common/btn_header_logo_sp.png" alt="Beeボーカルスクール"></a></h1>
            </div>
        </div>
    </div>
</header>
<div class="l-mvBlock">
    <div class="l-mv">
    	<div class="l-inner02">
            <p class="p-title">無料体験レッスンのお申し込み</p>
            <p class="p-image"><img src="../images/form/img_step02_sp.png" alt="確認"></p>
        </div>
    </div>
</div>
<div id="wrapper">
	<div id="contact" class="l-contactBlock">
    	<div>
    		<form method="post" action="../complete/" id="entry">
            <div class="slider">
                <div class="l-table namae">
                    <div class="l-title">                    	
                        <div class="p-title"><p>基本情報</p></div>
                    </div>                      
                    <table>
                        <tr>
                            <td>
                            	<p class="p-title">お名前</p>
                                <p class="p-data"><?php echo $_SESSION['namae']; ?></p>
                                <p class="p-title">ご年代</p>
                                <p class="p-data"><?php echo $_SESSION['age']; ?></p>
                               	<p class="p-title">電話番号</p>
                                <p class="p-data"><?php echo $_SESSION['tel']; ?></p>
                                <p class="p-title">メールアドレス</p>
                                <p class="p-data"><?php echo $_SESSION['mail']; ?></p>
                                <input type="hidden" class="p-namae2" value="<?php echo $_SESSION['namae']; ?>">
                                <input type="hidden" class="p-age2" value="<?php echo $_SESSION['age']; ?>">
                                <input type="hidden" class="p-tel2" value="<?php echo $_SESSION['tel']; ?>">
                                <input type="hidden" class="p-other2" value="<?php echo $_SESSION['mail']; ?>">
                            </td>
                        </tr>
                    </table>
                </div>                
                <div class="l-table area">                    
                    <div class="l-title">                    	
                        <div class="p-title"><p>レッスン情報</p></div>
                    </div>                     
                    <table>
                        <tr>
                            <td>
                            	<p class="p-title">希望エリア</p>
                                <p class="p-data">
                                	<?php if($area === '希望エリア'){ ?>
                                    	<?php echo $_SESSION['kibou_area']; ?>
                                    <?php } else { ?>
                                    	<?php echo $_SESSION['area']; ?>
                                    <?php } ?>
                                </p>
                                <p class="p-title">希望受講日時</p>
                                <p class="p-data">
                                	<?php if($jyukou === '日付を決める'){ ?>
                                    	第一希望：<br><?php echo $_SESSION['kibou1_1']; ?><br>
                                        第二希望：<br><?php echo $_SESSION['kibou2_1']; ?><br>
                                        第三希望：<br><?php echo $_SESSION['kibou3_1']; ?>
                                    <?php } else { ?>
                                    	<?php echo $_SESSION['jyukou']; ?>
                                    <?php } ?>
                                </p>
                                <p class="p-title">ご質問・ご要望</p>
                                <p class="p-data"><?php echo $_SESSION['other']; ?></p>
                            </td>
                        </tr>
                    </table>
                    <div class="l-button l-step05">
                        <p class="p-back">
                        	<a href="javascript:history.back();">戻る</a>
                        </p><p class="p-next">
                        	<input id="stepnextcomplete" type="submit" class="js-next" value="この内容で申し込む">
                        </p>
                    </div>
                </div>
            </div>
            </form>
    	</div>    
    </div>
</div>
<footer>
    <div>
        <p class="p-message">ボイストレーニング・ボイトレの<br>Beeボーカルスクール (新宿・渋谷・池袋・赤羽)</p>
        <p class="p-copy">Copyright &copy; 2009-2015 Bee Vocal School.<br>All Rights Reserved. </p>
    </div>
</footer>
</body>
</html>
