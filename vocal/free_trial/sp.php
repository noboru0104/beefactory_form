<!doctype html>
<html lang="ja">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WQNZJ8P');</script>
<!-- End Google Tag Manager -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="keywords" content="ボイストレーニング,ボイトレ,ボーカルスクール">
<meta name="description" content="ボイストレーニングなら、Beeボーカルスクール！経験豊富なインストラクターによるマンツーマンのボイトレで、初心者からプロ志望まで対応！新宿、池袋、渋谷、赤羽の駅近でアクセス良好です。">
<meta name="author" content="株式会社ビー・ファクトリー">
<meta name="copyright" content="Copyright © Bee Factory, Inc. All rights reserved.">
<meta property="og:title" content="ボイストレーニングのBeeボーカルスクール">
<meta property="og:type" content="website">
<meta property="og:url" content="https://www.bee-music.jp/vocal/">
<meta property="og:image" content="https://www.bee-music.jp/vocal/assets/img/og-image.jpg">
<meta property="og:site_name" content="ボイストレーニングのBeeボーカルスクール">
<meta property="og:description" content="ボイストレーニングなら、Beeボーカルスクール！経験豊富なインストラクターによるマンツーマンのボイトレで、初心者からプロ志望まで対応！新宿、池袋、渋谷、赤羽の駅近でアクセス良好です。">
<meta name="twitter:card" content="summary">
<meta name="twitter:description" content="ボイストレーニングなら、Beeボーカルスクール！経験豊富なインストラクターによるマンツーマンのボイトレで、初心者からプロ志望まで対応！新宿、池袋、渋谷、赤羽の駅近でアクセス良好です。">
<title>お問い合わせ｜ボイストレーニングのBeeボーカルスクール(新宿・池袋・渋谷)</title>
<!-- ▼共通CSS▼ -->
<link rel="stylesheet" type="text/css" href="css/common/reset.css">
<!-- ▲共通CSS▲ -->
<!-- ▼favicon▼ -->
<link rel="shortcut icon" href="images/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="icon" href="images/favicon.ico" type="image/vnd.microsoft.icon">
<!-- ▲favicon▲ -->
<!-- ▼個別CSS▼ -->
<link rel="stylesheet" type="text/css" href="css/contact.css">
<!-- ▲個別CSS▲ -->
<!-- ▼共通JS▼ -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common/script.js"></script>
<!-- ▲共通JS▲ -->
<!-- ▼個別JS▼ -->
<script type="text/javascript" src="js/common/jquery.matchHeight.js"></script>
<script type="text/javascript" src="js/contact/contact.js"></script>
<!-- ▲個別JS▲ -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQNZJ8P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header>    
    <div class="l-header">
        <div class="l-header-table">
            <div class="l-header-left">
                <h1><a href="/"><img src="images/common/btn_header_logo_sp.png" alt="Beeボーカルスクール"></a></h1>
            </div>
        </div>
    </div>
</header>
<div class="l-mvBlock">
    <div class="l-mv">
    	<img src="images/contact/img_mv_sp.jpg" alt="Beeボーカルスクール">
    </div>
</div>

<div id="wrapper">
    <section>
        <div class="l-contactBlock01">
            <div class="l-inner">          
                <p>
                	レッスンは実際のレッスンに近い<span>45分</span>のマンツーマンレッスンを体験いただけます！<br>
                	自分がどのコースか悩んでいる方でもお気軽にお申込みできます。<br>
                    どのようなことでもお気軽にご相談ください！
                </p>
            </div>
        </div>
        
        <div class="l-contactBlock02">
            <div class="l-inner">          
                <p class="p-title">
                	WEBでのお申し込みはこちら
                </p>
                <p class="p-button">
                	<a href="entry/"><img src="images/contact/btn_web_sp.png" alt="Webでのお申し込み 入会金も無料！"></a>
                </p>          
                <p class="p-title">
                	お電話でのお申し込みはこちら
                </p>
                <p class="p-button">
                	<a href="tel:0120-015-349"><img src="images/contact/btn_tel_sp.png" alt="0120-015-349 月～金 9:30～21:30 土日 9:30～19:30 タップで発信！"></a>
                </p>
            </div>
        </div>
    </section>
</div>
<footer>
    <div>
        <p class="p-message">ボイストレーニング・ボイトレの<br>Beeボーカルスクール (新宿・渋谷・池袋・赤羽)</p>
        <p class="p-copy">Copyright &copy; 2009-2015 Bee Vocal School.<br>All Rights Reserved. </p>
    </div>
</footer>

</body>
</html>
